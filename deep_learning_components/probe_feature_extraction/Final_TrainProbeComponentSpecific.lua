-- Probe Component Classifier
require 'torch'
require 'nn'
require 'nnx'
require 'cunn'
require 'cudnn'
require 'dpnn'
require 'dp'
require 'cutorch'

cudnn.benchmark = true
cudnn.fastest = true
cudnn.verbose = false

local cmd = torch.CmdLine()
cmd:text("Training of Probe Component Classifier")
cmd:text("")

-- listing the various options
cmd:option('--train_path','data/ProbeSegments4_WithTrainTestSplit/train','path containing the images for training')
cmd:option('--valid_path','data/ProbeSegments4_WithTrainTestSplit/valid','path containing the images for validation')
cmd:option('--test_path','data/ProbeSegments4_WithTrainTestSplit/test','path containing the images for testing')
cmd:option('--meta_path','data/ProbeSegments4_WithTrainTestSplit/meta','path containing the metadata such as mean and standard deviation')
cmd:option('--load_size','{3,128,128}','load image size')
cmd:option('--sample_size','{3,128,128}','sample image size')
cmd:option('--model','models/ProbeNetPretrained.lua','model to train')

cmd:option('--dropOutProb',0.5,'drop out rate')
cmd:option('--learningRate', 0.001, 'learning rate at t=0')
cmd:option('--lrDecay', 'adaptive', 'type of learning rate decay : adaptive | linear | schedule | none')
cmd:option('--minLR', 0.00001, 'minimum learning rate')
cmd:option('--saturateEpoch',100, 'epoch at which linear decayed LR will reach minLR')
cmd:option('--schedule', '{}', 'learning rate schedule')
cmd:option('--maxWait', 4, 'maximum number of epochs to wait for a new minima to be found. After that, the learning rate is decayed by decayFactor.')
cmd:option('--decayFactor', 0.001, 'factor by which learning rate is decayed for adaptive decay.')
cmd:option('--maxOutNorm', 1, 'max norm each layers output neuron weights')
cmd:option('--momentum', 0.9, 'momentum')
cmd:option('--hiddenSize', '{1000}', 'number of hidden units per layer')
cmd:option('--batchSize', 8 , 'number of examples per batch')
cmd:option('--cuda', true, 'use CUDA')
cmd:option('--useDevice', 1, 'sets the device (GPU) to use')
cmd:option('--maxEpoch', 30, 'maximum number of epochs to run')
cmd:option('--maxTries',20, 'maximum number of epochs to try to find a better local minima for early-stopping')
cmd:option('--dropout', false, 'apply dropout on hidden neurons')
cmd:option('--batchNorm', false, 'use batch normalization. dropout is mostly redundant with this')
cmd:option('--standardize', true, 'apply Standardize preprocessing')
cmd:option('--zca', false, 'apply Zero-Component Analysis whitening')
cmd:option('--progress', false, 'display progress bar')
cmd:option('--silent', false, 'dont print anything to stdout')
cmd:option('--weightDecay', 5e-4, 'weight decay')
cmd:option('--trainEpochSize', -1, 'number of train examples seen between each epoch')
cmd:option('--nThread', 0, 'allocate threads for loading images from disk. Requires threads-ffi.')

cmd:text()

local config = cmd:parse(arg or {})
config.schedule = dp.returnString(config.schedule)
config.hiddenSize = dp.returnString(config.hiddenSize)
config.load_size = dp.returnString(config.load_size)
config.sample_size = dp.returnString(config.sample_size)

-- create meta path
if paths.mkdir(config.meta_path) then print('meta directory created'); else print('meta directory already present') end

-- load the datasource
paths.dofile('ProbeCompImgSource.lua')
ds = dp.ProbeCompImgSource(config)

print(ds:classes())
trainSet = ds:trainSet()

print(trainSet)
--batch_inputs = trainSet:sample(10)
--dv = batch_inputs:inputs()
--print(dv:forwardGet('bchw'))
ppf = ds:normalizePPF()

cutorch.setDevice(config.useDevice)

config.outputSize = #ds:classes()
config.useCompleteModel = true

print('Configuration of Training')
table.print(config)

------------------------------ NETWORK DEFINITION ---------------------------
paths.dofile(config.model)
print(config.model)
net = create_model(config)
net:insert(nn.Convert(ds:ioShapes(),'bchw'),1)

-- using cudnn TODO:maybe write up constructors later on
--cudnn.convert(net,cudnn)
-- TODO: Replace ReLU units with RReLU units, or LeakyReLU(a = 100) or LeakyReLU(a = 5.5)
cudnn.convert(net, cudnn, function(module)
   return torch.type(module):find('SpatialBatchNormalization')
end)
net:replace(function(module)
   if torch.typename(module) == 'cudnn.ReLU' then
      return nn.RReLU()
   else
      return module
   end
end)

net = net:cuda()
print(net)
print(cudnn.version)
------------------------- END NETWORK DEFINITION ----------------------------

------------------ TRAINING AND EVALUATION ---------------------------------
if config.lrDecay == 'adaptive' then
  ad =dp.AdaptiveDecay{max_wait = config.maxWait, decay_factor = config.decayFactor}
elseif config.lrDecay == 'linear' then
  config.decayFactor = (config.minLR - config.learningRate)/config.saturateEpoch
end

-- Define the Optimizer for ,Evaluator for validation and testing
train = dp.Optimizer{
  acc_update = config.accUpdate,
  -- define loss
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),

  -- function to be called every epoch
  epoch_callback = function(model,report)
    -- update the learning rate after one iteration of all batches
    if report.epoch > 0 then
      if config.lrDecay == 'adaptive' then
        config.learningRate = config.learningRate *ad.decay
        ad.decay = 1
      elseif config.lrDecay == 'schedule' and config.schedule[report.epoch] then
        config.learningRate = config.schedule[report.epoch]
      elseif config.lrDecay == 'linear' then
        config.learningRate = config.learningRate + config.decayFactor
      end
      config.learningRate = math.max(config.minLR, config.learningRate)
      if not config.silent then
        print("learningRate",config.learningRate)
      end
    end
  end,

  -- call back function for every batch
  -- TODO: Maybe just use the optim libraries to optimize
  -- put here, to just re-train the last few layers
  -- the weights the other layers remain constant.
  callback = function(model,report)
    if config.accUpdate then
      model:accUpdateGradParameters(model.dpnn_input,model.output,config.learningRate)
    else
      model:updateGradParameters(config.momentum) -- affects gradients of the weights
      model:weightDecay(config.weightDecay)
      model:updateParameters(config.learningRate)
    end
    model:maxParamNorm(config.maxOutNorm) -- normalize the outputs of each neuron at each layer
    model:zeroGradParameters() -- zeros the gradients of the weights
  end,
  feedback = dp.Confusion(), -- provides accuracy
  sampler = dp.RandomSampler{batch_size = config.batchSize,epoch_size=config.trainEpochSize, ppf=ppf}, -- shuffle the indices
  progress = config.progress
}

-- Now the evaluator for the validation and test set
valid = dp.Evaluator{
  -- define loss
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),
  feedback = dp.Confusion(),
  sampler = dp.Sampler{batch_size = config.batchSize,ppf=ppf}
}

test = dp.Evaluator{
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),
  feedback = dp.Confusion(),
  sampler = dp.Sampler{batch_size = config.batchSize,ppf=ppf}
}

--[[multithreading]]--
if config.nThread > 0 then
   ds:multithread(config.nThread)
   train:sampler():async()
   valid:sampler():async()
end

-- setting up the experiment
xp = dp.Experiment{
  model = net,
  optimizer = train,
  validator = valid,
  tester = test,
  observer = {
    dp.FileLogger(),
    dp.EarlyStopper{
      error_report = {'validator','feedback','confusion','accuracy'},
      maximize = true,
      max_epochs = config.maxTries
    }
  },
  random_seed = os.time(),
  max_epoch = config.maxEpoch
}

print(xp)
xp:cuda()
xp:run(ds)
