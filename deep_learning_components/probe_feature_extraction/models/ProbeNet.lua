-- Probe Net
------------------------------ NETWORK DEFINITION ---------------------------
-- define the Fully connected network
-- TODO: Define the network here, but later define in another file which can be read.
-- code reference : https://github.com/facebook/fb.resnet.torch/blob/master/models/resnet.lua
local function basicResidualBlock(nIn,nOut,stride)
  -- convolution branch
  local resBlock_br1 = nn.Sequential()

  -- adding the layer
  resBlock_br1:add(nn.SpatialConvolution(nIn,nOut,3,3,stride,stride,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))
  resBlock_br1:add(nn.ReLU())
  resBlock_br1:add(nn.SpatialConvolution(nOut,nOut,3,3,1,1,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))

  -- indentity branch

  -- Create a layer to house the two branches
  local resBlock = nn.ConcatTable()
  resBlock:add(resBlock_br1)
  if nIn ~= nOut then
    resBlock:add(nn.SpatialConvolution(nIn,nOut,1,1,stride,stride))
    --resBlock_br2:add(cudnn.SpatialBatchNormalization(nOut))
  else
    resBlock:add(nn.Identity())
  end

  local finalResBlock = nn.Sequential()
  finalResBlock:add(resBlock)
  finalResBlock:add(nn.CAddTable(true))
  finalResBlock:add(nn.ReLU(true))

  return finalResBlock
end

function create_model(numOutputs)
  -- define local functions which define the residual layer (basic block
  local net = nn.Sequential()

  -- adding the first convolutional layers which extracts raw features
  -- input : 3 x 224 x 224 ; output: 64 x 56 x 56
  net:add(nn.SpatialConvolution(3,64,7,7,4,4,3,3))
  net:add(nn.SpatialBatchNormalization(64))
  net:add(nn.ReLU())

  -- adding the second convolutional layer
  -- input : 64 x 56 x 56 ; output: 128 x 28 x 28
  net:add(nn.SpatialConvolution(64,128,5,5,2,2,2,2))
  net:add(nn.SpatialBatchNormalization(128))
  net:add(nn.ReLU())

  -- adding the third convolutional layer
  -- input : 128 x 28 x 28 ; output: 256 x 14 x 14
  net:add(nn.SpatialConvolution(128,256,3,3,2,2,1,1))
  net:add(nn.SpatialBatchNormalization(256))
  net:add(nn.ReLU())

  -- adding the max pooling layer
  net:add(nn.SpatialConvolution(256,256,3,3,2,2,1,1))
  net:add(nn.ReLU())
  net:add(nn.SpatialAveragePooling(7,7,1,1))

  -- output is 256 element vector
  net:add(nn.View(256))
  net:add(nn.Linear(256,500))
  net:add(nn.ReLU())
  net:add(nn.Linear(500,numOutputs))
  net:add(nn.LogSoftMax())

  return net

end