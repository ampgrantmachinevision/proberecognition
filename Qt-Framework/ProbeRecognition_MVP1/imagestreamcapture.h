#ifndef IMAGESTREAMCAPTURE_H
#define IMAGESTREAMCAPTURE_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDir>
#include <opencv2/opencv.hpp>
#include <QTextStream>
#include "common_variables.h"

typedef struct{
    QString name; // camera name or "File"
    QString location; // IP address or Image Directory
    QString type; // format of image -jpg, png
    // maybe some parameters
} CameraType;

class ImageStreamCapture : public QObject
{
    Q_OBJECT
public:
    explicit ImageStreamCapture(CameraType cam, QObject *parent = 0);
    void streamCapture(int temporal_length, QString class_name = "", bool display = true);
    void imageDisplay(cv::Mat img, int time_out);
    void saveImgBufferToFiles(QString class_name = "");
    void clearImgBuffer();
    void setCam(const CameraType &value);
    std::vector<cv::Mat> imgbuffer(int count) const;
    void imageResize(cv::Mat& img, int scale = 1);

    void getStreamingImages(int temporal_length,QString class_name = "");

signals:

public slots:

protected:
    int m_temporallength;
    std::vector<cv::Mat> m_imgbuffer;
    CameraType cam;
};

#endif // IMAGESTREAMCAPTURE_H
