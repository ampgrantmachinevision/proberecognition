-- Creationg of Data source from the extracted features using DP method.
require 'torch'
require 'torchx'
require 'dp'

cmd = torch.CmdLine()

-- listing the various options
cmd:option('-features','data/features-all.t7','path containing the features for training')
cmd:option('-labels','data/component_labels-all.t7','path containing the labels for training')
cmd:option('--validRatio',0.2,'Percentage of samples to be used for validation')
cmd:option('--testRatio',0.2,'Percentage of samples to be used for testing')
cmd:option('--dataSourceName','ProbeComponentDataSource','Name of the datasource')

-- parse the options
config = cmd:parse(arg)

-- load the features
features = torch.load(config.features)
labels = torch.load(config.labels)

local num_samples = 0
local num_dims = features[1]:size(2)

-- Iterate through samples
for i, v in ipairs(features) do
  num_samples = num_samples + v:size(1)
end

-- combine table of features into 1 Tensor
local input = torch.FloatTensor(num_samples,num_dims)
local target = torch.IntTensor(num_samples,1)
local s_input = torch.FloatTensor(num_samples,num_dims)
local s_target = torch.IntTensor(num_samples,1)

-- iterate again through samples to populate input and target
cum_idx = 1
for i,v in ipairs(features) do
  local labels_per_img = labels[i]
  local features_per_img = v:clone()
  local num_samples_per_img = v:size(1)

  input[{ {cum_idx,cum_idx+num_samples_per_img-1},{1,num_dims} }] = v
  target[{ {cum_idx,cum_idx+num_samples_per_img-1}, 1 }] = labels_per_img

  cum_idx = cum_idx + num_samples_per_img

  collectgarbage()
end

print(#input)
print(#target)

-- create a shuffled index
shuffle = torch.randperm(num_samples)

-- shuffle the inputs and target
for i = 1,1,num_samples do
  idx = shuffle[i]
  s_input[{ {idx}, {} }]:copy(input[i])
  s_target[{ {idx}, {} }]:copy(target[i])
end

print(#s_input)
print(#s_target)

-- TODO: DIVIDE INTO TRAIN, VALIDATION AND TEST SET
local nValid = math.floor(num_samples*opt.validRatio)
local nTest = math.floor(num_samples*opt.testRatio)
local nTrain = size - nValid - nTest

local trainSet =

