#include "imagestreamcapture.h"

ImageStreamCapture::ImageStreamCapture(CameraType cam, QObject *parent): QObject(parent)
{
    this->setCam(cam);
}

void ImageStreamCapture::streamCapture(int temporal_length, QString class_name, bool display)
{
    if(cam.name == "File"){
        // read the classes text file to obtain directories where each directory contains corresponding to a probe type
        QString img_location = cam.location;
        img_location += "/";
        img_location += class_name;

        // setting the filters and name filters for retreiving images
        QDir dir(img_location);
        //std::cout << img_location.toStdString() << std::endl;
        QStringList filters;
        filters << "*.jpg" << "*.png" << "*.tiff";
        dir.setNameFilters(filters);
        //dir.setFilter(QDir::Files);

        QStringList imgList = dir.entryList();
        int count = 0;
        if(temporal_length == -1)
            temporal_length = imgList.size();

        for(QStringList::iterator it = imgList.begin(); it!= imgList.end(); ++it){
            QString img_path_absolute = img_location + "/" + (*it);
            std::string img_name = img_path_absolute.toStdString();
            cv::Mat img = cv::imread(img_name);

            // resize image to be compatible with standard ImageNET input size
            this->imageResize(img);

            this->m_imgbuffer.push_back(img);
            count++;
            if(display)
                this->imageDisplay(img,10);
            if(count <= temporal_length)
                break;
        }

        std::cout << "Image list read" <<std::endl;

    }
    else if(cam.name == "PointGrey"){

    }

}

void ImageStreamCapture::imageDisplay(cv::Mat img, int time_out)
{
    // display sample image
    cv::namedWindow(INPUT_WIN,cv::WINDOW_NORMAL);
    cv::imshow(INPUT_WIN,img);
    cv::waitKey(time_out);
}

void ImageStreamCapture::saveImgBufferToFiles(QString class_name)
{
    QString resized_img_location = cam.location;
    resized_img_location += "/";
    resized_img_location += RESIZED_IMG_FOLDER;
    resized_img_location += "/";
    resized_img_location += class_name;


}

void ImageStreamCapture::clearImgBuffer()
{
    this->m_imgbuffer.clear();
}


void ImageStreamCapture::setCam(const CameraType &value)
{
    cam = value;
}

std::vector<cv::Mat> ImageStreamCapture::imgbuffer(int count) const
{
    if(count == -1 || count >= m_imgbuffer.size() ){
        return m_imgbuffer;
    }
    else if(count < m_imgbuffer.size()){
        std::vector<cv::Mat>::const_iterator first = m_imgbuffer.begin();
        std::vector<cv::Mat>::const_iterator second = m_imgbuffer.begin() + count;
        std::vector<cv::Mat> new_imgBuffer(first,second);
        return new_imgBuffer;
    }
}

void ImageStreamCapture::imageResize(cv::Mat &img, int scale)
{
    int no_rows = std::floor(img.rows/IMAGENETSIZE);
    int no_cols = std::floor(img.cols/IMAGENETSIZE);

    cv::Size img_size = cv::Size(no_cols * IMAGENETSIZE/scale,no_rows* IMAGENETSIZE/scale);

    cv::resize(img,img,img_size);
}

void ImageStreamCapture::getStreamingImages(int temporal_length, QString class_name)
{
    // stream capture - from a specific class
    this->streamCapture(temporal_length, class_name, true); // only passing a single frame
}
