-- Creationg of Data source from the extracted features using DP method.
require 'torch'
require 'torchx'
require 'dp'

cmd = torch.CmdLine()

-- listing the various options
cmd:option('--train_path','data/images/train','path containing the images for training')
cmd:option('--valid_path','data/images/valid','path containing the images for validation')
cmd:option('--test_path','data/images/test','path containing the images for testing')
cmd:option('--meta_path','data/images/meta','path containing the metadata such as mean and standard deviation')
cmd:option('--load_size','{3,64,64}','load image size')
cmd:option('--sample_size','{3,64,64}','sample image size')
--cmd:option('-load_size','{3,64,64}','Table showing the load size of the structure')


-- parse the options
config = cmd:parse(arg)
config.load_size = dp.returnString(config.load_size)
config.sample_size = dp.returnString(config.sample_size)

print(config)

--paths.dofile('ProbeCompImgSource.lua')

--ds = dp.ProbeCompImgSource(config)

ds = dp.ImageSource(config)
print(ds:classes())
trainSet = ds:trainSet()

print(trainSet)
batch_inputs = trainSet:sample(10)
dv = batch_inputs:inputs()
print(dv:forwardGet('bchw'))

--print(#ds:trainset())

---- load the class file
--classes_file = torch.DiskFile(config.setOfClasses,"r")
--classes_file:quiet()
--num = 1
--set_of_classes = {}
--repeat
--  classes_str = classes_file:readString("*l")
--  set_of_classes[num] = classes_str
--  num = num+1
--until classes_str == ''
--set_of_classes[num-1] = nil

--print('Set of classes:')
--print(set_of_classes)

---- load the features
--print('Loading features')
--glimpses = torch.load(config.glimpses)
--labels = torch.load(config.labels)
--print('features loaded')

--local num_samples = 0
--local num_channels = glimpses[1]:size(2)
--local num_rows = glimpses[1]:size(3)
--local num_cols = glimpses[2]:size(4)

---- Iterate through samples
--for i, v in ipairs(glimpses) do
--  num_samples = num_samples + v:size(1)
--end

---- combine table of features into 1 Tensor
--local input = torch.FloatTensor(num_samples,num_channels,num_rows,num_cols)
--local target = torch.IntTensor(num_samples)
--local s_input = torch.FloatTensor(num_samples,num_channels,num_rows,num_cols)
--local s_target = torch.IntTensor(num_samples)

---- iterate again through samples to populate input and target
--cum_idx = 1
--for i,v in ipairs(glimpses) do
--  local labels_per_img = labels[i]
--  local features_per_img = v:clone()
--  local num_samples_per_img = v:size(1)

--  input[{ {cum_idx,cum_idx+num_samples_per_img-1},{},{},{} }] = v
--  target[{ {cum_idx,cum_idx+num_samples_per_img-1}}] = labels_per_img

--  cum_idx = cum_idx + num_samples_per_img

--  collectgarbage()
--end

--print(#input)
--print(#target)

---- create a shuffled index
--shuffle = torch.randperm(num_samples)

---- shuffle the inputs and target
----for i = 1,1,num_samples do
----  idx = shuffle[i]
----  s_input[{ {idx}, {} }]:copy(input[i])
----  s_target[{idx}]:copy(target[i])
----end

--s_input = input:clone()
--s_target = target:clone()

--print(#s_input)
--print(#s_target)

---- TODO: DIVIDE INTO TRAIN, VALIDATION AND TEST SET
--local nValid = math.floor(num_samples*config.validRatio)
--local nTest = math.floor(num_samples*config.testRatio)
--local nTrain = num_samples - nValid - nTest

--local trainInput = dp.ImageView('bchw',s_input:narrow(1,1,nTrain))
--local trainTarget = dp.ClassView('b',s_target:narrow(1,1,nTrain))

--local validInput = dp.ImageView('bchw',s_input:narrow(1,nTrain+1,nValid))
--local validTarget = dp.ClassView('b',s_target:narrow(1,nTrain+1,nValid))

--local testInput = dp.ImageView('bchw',s_input:narrow(1,nTrain+nValid+1,nTest))
--local testTarget = dp.ClassView('b',s_target:narrow(1,nTrain+nValid+1,nTest))

--trainTarget:setClasses(set_of_classes)
--validTarget:setClasses(set_of_classes)
--testTarget:setClasses(set_of_classes)

-- wrap into dataset views
-- TODO : Instead of dp.DataSet, I would use dp.ImageClassSet which will index directly from the file directory structure

-- TODO: create different ImageClassSets for train, valid, and test

--local train = dp.DataSet{inputs=trainInput, targets = trainTarget, which_set='train'}
--local valid = dp.DataSet{inputs=validInput, targets = validTarget, which_set='valid'}
--local test = dp.DataSet{inputs=testInput, targets = testTarget, which_set='test'}

--local input_preprocess = {}
--table.insert(input_preprocess, dp.Standardize())

--local ds = dp.DataSource{train_set = train, valid_set = valid, test_set=test,input_preprocess = input_preprocess}
--ds:classes(set_of_classes)

--print(ds)

--torch.save(config.dataSourceFile,ds)

--print('Operation completed successfully')
--print('Printing out sample data')
--tensor_train = ds:get('train', 'input', 'default')
--tensor_test = ds:get('test', 'input', 'default')
--tensor_valid = ds:get('valid','input','default')

--print(#tensor_train)
--print(#tensor_test)
--print(#tensor_valid)