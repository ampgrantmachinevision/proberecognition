-- Probe Component Classification Network
require 'torch'
require 'nn'
require 'nnx'
require 'cunn'
require 'cudnn'

-- define a class which takes in features, and labels and trains a fully-connected network, not fine-tuning the big network
local ProbeComponentNet,_ = torch.class('nn.ProbeComponentNet','nn.Container')

-- function constructor
-- define the network for the probe component classifier
function ProbeComponentNet:__init(config)

  -- define the network
  self:defineNet(config)

  -- print the network
  self:printNet()

end

function ProbeComponentNet:defineNet(config)
  -- define local functions which define the residual layer (basic block)
  -- code reference : https://github.com/facebook/fb.resnet.torch/blob/master/models/resnet.lua
  local function basicResidualBlock(nIn,nOut,stride)

    -- convolution branch
    local resBlock_br1 = nn.Sequential()

    -- adding the layer
    resBlock_br1:add(cudnn.SpatialConvolution(nIn,nOut,3,3,stride,stride,1,1))
    resBlock_br1:add(nn.SpatialBatchNormalization(nOut))
    resBlock_br1:add(cudnn.ReLU())
    resBlock_br1:add(cudnn.SpatialConvolution(nOut,nOut,3,3,1,1,1,1))
    resBlock_br1:add(nn.SpatialBatchNormalization(nOut))

    -- indentity branch
    local resBlock_br2 = nn.Sequential()
    if nIn ~= nOut then
      resBlock_br2:add(cudnn.SpatialConvolution(nIn,nOut,1,1,stride,stride))
      resBlock_br2:add(cudnn.SpatialBatchNormalization(nOut))
    else
      resBlock_br2:add(nn.Identity())
    end

    -- Create a layer to house the two branches
    local resBlock = nn.ConcatTable()
    resBlock:add(resBlock_br1)
    resBlock:add(resBlock_br2)

    local finalResBlock = nn.Sequential()
    finalResBlock:add(resBlock)
    finalResBlock:add(nn.CAddTable(true))
    finalResBlock:add(cudnn.ReLU(true))

    return finalResBlock
  end

--  -- creates a specific residual layer with number of residual blocks
--  local function resLayer(nIn,nOut, count, stride)
--    local resBlocks = nn.Sequential()
--    for i=1,count do
--      resBlocks:add(basicResidualBlock(nIn,nOut, i == 1 and stride or 1))
--    end
--    return resBlocks
--  end

  local model
  if(config.useCompleteModel == true) then
    -- Load the pre-trained model
    model = torch.load('pretrained/resnet-18.t7')

    -- remove the fully connected layer
    model:remove(#model.modules)
    model:remove(#model.modules)
    model:remove(#model.modules)

    -- remove the last two residual layers which models block by block relations (conv#4 and conv#5)
    model:remove(#model.modules)
    model:remove(#model.modules)

  else
    model = nn.Sequential()
    -- output of feature map is now b x 128 x 28 x 28
  end

  -- define 4th conv residual layers
  model:add(basicResidualBlock(128,256,2))
  model:add(basicResidualBlock(256,256,1))

  -- define the 5th conv residual layer
  model:add(basicResidualBlock(256,512,2))
  model:add(basicResidualBlock(512,512,1))

  -- add the spatial average pooling filter
  model:add(nn.SpatialAveragePooling(7,7,1,1))
  model:add(nn.View(512):setNumInputDims(3))

  -- add the fully connected layer
  local num_hiddenUnits
  local num_outputs = config.numOutputs
  for i,v in ipairs(config.hiddenSize) do
    num_hiddenUnits = v
    model:add(nn.Linear(num_inputs,num_hiddenUnits))
    model:add(nn.ReLU())
  end
  net:model(nn.Linear(num_hiddenUnits,num_outputs))
  net:model(nn.LogSoftMax())

  self.model = model:cuda()
  return model
end

function ProbeComponentNet:getModel()
  return self.model
end

function ProbeComponentNet:printNet()
  print('Network Definition')
  print(self.model)
end
-- function for training the network : Use standard trainer, and then experiment with optim.
--------------------------------------------------------------------------------
-- function: training
function ProbeComponentNet:training()
  self.model:training();
end

--------------------------------------------------------------------------------
-- function: evaluate
function ProbeComponentNet:evaluate()
  self.model:evaluate();
end

--------------------------------------------------------------------------------
-- function: to cuda
function ProbeComponentNet:cuda()
  self.model:cuda();
end

--------------------------------------------------------------------------------
-- function: to float
function ProbeComponentNet:float()
  self.model:float();
end

--------------------------------------------------------------------------------
-- function: inference (used for full scene inference)
function ProbeComponentNet:inference()
  self.model:evaluate()
  self:cuda()
end

-- function for inference : which outputs a class label
return nn.ProbeComponentClassifier