-- Training of neural network
-- As of now, one file but later split the code into multiple files
require 'torch'
require 'optim'
require 'nn'
require 'paths'
require 'cutorch'
require 'cunn'
require 'cudnn'
require 'image'

cudnn.benchmark = true
cudnn.fastest = true
cudnn.verbose = false

torch.setdefaulttensortype('torch.FloatTensor')

--------------------------- SETIING UP OPTIONS ------------------------------
-- Setting upt the options
local cmd = torch.CmdLine()
cmd:text("Training of Probe Component Classifier")
cmd:text("")

-- options for selecting the train and text files : specified in train.txt and test.txt with labels
cmd:option('-trainFile','data/train_probe.txt','File containing the paths to the training images(already shuffled)')
cmd:option('-validRatio',0.15,'ratio for selecting the validation ratio')
cmd:option('-classesFile','data/probe_component_classes.txt','list of probe component classes')

-- options for the training
cmd:option('numEpochs',50,'Maximum number of epochs')
cmd:option('batchSize',128,'Size of each batch')
cmd:option('minEpochDecay',2,'The epoch number after which the decay starts')

-- options for optimization (for optim)
cmd:option('-learningRate',0.01,'learning rate at t=0')
cmd:option('-momentum',0.5,'Momentum')
cmd:option('-weightDecay',5e-4,'weight decay')
cmd:option('-saturateEpoch',50,'Epoch by which learning rate will reach minimum')
cmd:option('-minLearningRate',0.00001,'The minimum learning rate')
cmd:option('-decayFactor',0.001,'Factor by which learning rate is decayed for adaptive decay')
cmd:option('-lrDecayType','adaptive','type of learning rate decay : adaptive | linear | schedule | none')

-- options to log results
cmd:option('-trainLogFile','data/train_stats.log','Log file for training')

-- options to select the model
cmd:option('-model','models/ProbeNet.lua','Model definition')

cmd:text()
local inputSize = torch.Tensor(3):zero()
inputSize[1] = 3; inputSize[2] = 224; inputSize[3] = 224
local opt = cmd:parse(arg or {})

------------------------------ OPTIONS END -----------------------------------

------------------------- READING FILES, CONVERSION TO BATCHES ---------------

-- load the class file
local classes_file = torch.DiskFile(opt.classesFile,"r")
classes_file:quiet()
num = 1
local set_of_classes = {}
repeat
  classes_str = classes_file:readString("*l")
  set_of_classes[num] = classes_str
  num = num+1
until classes_str == ''
set_of_classes[num-1] = nil

-- Read train files
local train_file = io.open(opt.trainFile)
-- read all the train file names
local count = 1
local img_files = {}
local img_class_labels = {}
while true do
  local line = train_file:read()
  if line == nil then
    break
  end
  --print(line)
  local k = string.find(line,'\t')
  local img_file_name = string.sub(line,1,k-1)
  local img_class = string.sub(line,k+1,-1)

  img_files[count] = img_file_name
  img_class_labels[count] = img_class

  count = count + 1
end
train_file:close(train_file)

local num_samples = #img_files
local num_valid_samples = torch.ceil(opt.validRatio * num_samples)
local num_train_samples = num_samples - num_valid_samples

local list_of_train_files = {}; local list_of_train_labels = {}
local list_of_valid_files = {}; local list_of_valid_labels = {}

-- sort training and test labels
for i,v in ipairs(img_files) do
  if(i <= num_train_samples) then
    list_of_train_files[i] = v
  else
    list_of_valid_files[i-num_train_samples] = v
  end
end

for i,v in ipairs(img_class_labels) do
  if(i <= num_train_samples) then
    list_of_train_labels[i] = v
  else
    list_of_valid_labels[i-num_train_samples] = v
  end
end

-- Define the function to compute the mean and variance
local function computeMeanVarPerFeature(list_of_train_files)
  local sum = torch.CudaTensor(inputSize[1],inputSize[2],inputSize[3]):zero()
  local sum2 = torch.CudaTensor(inputSize[1],inputSize[2],inputSize[3]):zero()

  -- computing mean
  for i,v in ipairs(list_of_train_files) do
    local img_filename = v
    local img = image.load(img_filename)

    sum =  sum + img:cuda() -- sum
    sum2 = sum + torch.cmul(img:cuda(),img:cuda()) --  sum squared
  end

  local mean = sum / num_train_samples

  -- var = sum2 - (torch.cmul(sum,sum)/num_train_samples)/(num_train_samples-1)
  local var = (sum2 - (torch.cmul(sum,sum)/num_train_samples))/(num_train_samples-1)
  local std = torch.sqrt(var)

  return mean,std
end

-- comute mean and variance
local meanImg,stdImg
if not (paths.filep('data/meanImg.png') and paths.filep('data/stdImg.png')) then
  meanImg,stdImg = computeMeanVarPerFeature(list_of_train_files)
  nn.utils.recursiveType(meanImg,'torch.FloatTensor')
  nn.utils.recursiveType(stdImg,'torch.FloatTensor')
  collectgarbage()

  image.save('data/meanImg.png',meanImg)
  image.save('data/stdImg.png',stdImg)
  print('Mean and Std computed')
else
  meanImg = image.load('data/meanImg.png')
  stdImg = image.load('data/stdImg.png')
  print('Mean and Std already present')
end


-- Function to normalize
local function normalizeImg(img,mean,std)
  num_c = img:size(1)
  for i=1,num_c,1 do
    img[i]:add(-mean[i])
    img[i]:cdiv(std[i])
  end
  return img
end


---- TODO: Function to compute global mean and standard deviation
--local function computeGlobalMeanVar(list_of_train_files)
--  local sum = 0
--  local sum2 = 0

--  -- computing mean
--  for i,v in ipairs(list_of_train_files) do
--    local img_filename = v
--    local img = image.load(img_filename)

--    sum =  sum + img:sum()

--    sum2 = sum + torch.cmul(img,img)
--  end

--  local mean = sum / num_train_samples

--  -- var = sum2 - (torch.cmul(sum,sum)/num_train_samples)/(num_train_samples-1)
--  local var = (sum2 - (torch.cmul(sum,sum)/num_train_samples))/(num_train_samples-1)

--  return mean,var

--end

-- TODO: Define the function to normalize them using mean and variance

-- define function to read a batch of images and output a tensor
local function readBatchOfImages(list_of_files,list_of_labels,offset,inputSize)
  local batchInputs = torch.FloatTensor(opt.batchSize,inputSize[1],inputSize[2],inputSize[3])
  local batchLabels = torch.FloatTensor(opt.batchSize)

  -- TODO: Do a hack to take care of last batch which may not contain batch size inputs
  for i = 1,opt.batchSize,1 do
    local img_filename = list_of_files[offset + i]
    if img_filename == nil then
      print(img_filename)
    end
    local img = image.load(img_filename)

    -- normalize img
    img_norm = normalizeImg(img,meanImg,stdImg)

    batchInputs[{ {i},{},{},{} }] = img_norm
    batchLabels[{i}] = list_of_labels[i]
  end

  return batchInputs,batchLabels

end

------------------------- END READING FILES ----------------------------------

------------------------- OPTIMIZATION PARAMETER/LOGGER SETUP------------------------

-- Initialize optimState
local optimState = {
  learningRate = opt.learningRate,
  learningRateDecay = 0.0,
  momentum = opt.momentum,
  dampening = 0.0,
  weightDecay = opt.weightDecay
}

-- create the logger for the train file
trainLogger = optim.Logger(opt.trainLogFile)

------------------------- END OPTIMIZATION/LOGGER PARAMETER -------------------------

------------------------  NETWORK DEFINITION AND CRITERIA --------------------
paths.dofile(opt.model)
print(opt.model)
net = create_model(#set_of_classes)

-- using cudnn TODO:maybe write up constructors later on
cudnn.convert(net,cudnn)
net = net:cuda()

-- CREATE THE CRITERION
criterion = nn.ClassNLLCriterion()
print('Model : ')
print(net)

print('Criterion : ')
print(criterion)

criterion:cuda()

---- find the number of parameters
--params = net:parameters()
--n_params = 0
--for k,v in pairs(params) do n_params = n_params+v:nElement() end
--mesg = string.format('Number of parameters : %d\n',n_params)
--print(mesg)

collectgarbage()

------------------------- END NETWORK DEFINITION ----------------------------

------------------ TRAINING AND EVALUATION ---------------------------------
function experiment()
  print('Running the experiment')

  -- iterate through each epoch
  for epoch=1,opt.numEpochs do

    -- call the train function
    train(epoch,opt,optimState)

    -- call the test function
    test(epoch,opt)

    -- print out statistics of both

  end

end

-- train for each epoch
function train(epoch,opt,optimState)
  local numBatches = torch.floor(num_train_samples/opt.batchSize)

  -- hack for 1 batch.
  --numBatches = 10
  local lastBatchNum = num_train_samples - numBatches * opt.batchSize

  -- function to update learning rate adatively after each iteration/epoch
  local function LR_epoch(epoch,opt,optimState)
    if opt.lrDecayType == 'linear' then
      optimState.learningRate = optimState.learningRate - (optimState.learningRate - opt.minLearningRate)/opt.saturateEpoch
    elseif opt.lrDecayType == 'adaptive' then
      optimState.learningRate = optimState.learningRate/(1 + opt.decayFactor * epoch)
    end
    return optimState
  end

  -- Change learning rate adaptively after a minimum number of iterations
  if epoch > opt.minEpochDecay then
    optimState = LR_epoch(epoch,opt,optimState)
  end

  -- set the model for training
  net:training()

  local acc_top1_epoch = 0
  local err_epoch = 0

  -- Iterate through each batch of images
  for b=1,numBatches,1 do

    -- read the batch of inputs
    local offset = (b-1)*opt.batchSize
    local batchInputs,batchLabels = readBatchOfImages(list_of_train_files,list_of_train_labels,offset,inputSize)

    cutorch.synchronize()

    -- train the network using the batch of images
    local err_batch,top1_batch = trainBatch(batchInputs,batchLabels)
    local acc_top1_batch = top1_batch * 100 / opt.batchSize

     -- Calculate top-1 error, and print information
    local mesg = string.format('Epoch: [%d][%d/%d]\t Err %f Top1-%%: %f LR %f',epoch, b,numBatches,err_batch, acc_top1_batch,optimState.learningRate)
    print(mesg)

    err_epoch = err_epoch + err_batch
    acc_top1_epoch = acc_top1_epoch + top1_batch
  end

  -- Logging the result for each epoch
  acc_top1_epoch = acc_top1_epoch * 100 / (opt.batchSize * numBatches)
  err_epoch = err_epoch / numBatches
  trainLogger:add{
      ['% top1 accuracy (train set)'] = acc_top1_epoch,
      ['avg loss (train set)'] = err_epoch
  }

  -- printing out the result
  print(string.format('Epoch: [%d][TRAINING SUMMARY]\t'
                          .. 'average loss (per batch): %f \t '
                          .. 'accuracy(%%):\t top-1 %f\t',
                       epoch, err_epoch, acc_top1_epoch))
  print('\n')

  -- save model
  collectgarbage()

  net:clearState()

  -- TODO: Save intermediate trained model for each epoch and the optimization state

end

local parameters,gradParameters = net:getParameters()

-- TODO: training each batch of images with parameter update
function trainBatch(batchInputs,batchLabels)

  --cutorch.synchronize()
 -- collectgarbage()

  -- convert batch inputs to cuda format
  local batchInputs_cuda = batchInputs:cuda()
  local batchLabels_cuda = batchLabels:cuda()

  local err, top1
  local outputs
  --define a function for optimization
  function feval(params)
    -- zero grad parameters
    net:zeroGradParameters()

    -- get the outputs
    outputs = net:forward(batchInputs_cuda)

    -- get the output error
    err = criterion:forward(outputs,batchLabels_cuda)

    -- compute the gradients
    local gradOutputs = criterion:backward(outputs, batchLabels_cuda)

    -- do backpropagation
    net:backward(batchInputs_cuda,gradOutputs)

    -- return the err and gradParameters
    return err,gradParameters

  end

  -- now call the optim package to update parameters
  optim.sgd(feval,parameters,optimState)

  -- synchronize
  --cutorch.synchronize()

  -- calculate top1 accuracy
  local _,predicted_label = outputs:float():sort(2,true) -- descending order
  top1 = 0
  for i=1,opt.batchSize do
    if predicted_label[i][1] == batchLabels[i] then
      top1 = top1 + 1
    end
  end

  collectgarbage()

  return err,top1

end

-- TODO: Write the test model
function test(epoch,opt)

  -- set the neural network for evaluation
  net:evaluate()

  -- get batches of test data
  local numBatches = torch.floor(num_valid_samples/opt.batchSize)
  local lastBatchNum = num_valid_samples - numBatches * opt.batchSize

  -- Hack
  --numBatches = 2

  -- initializing local error
  local acc_top1_epoch = 0
  local err_epoch = 0

  -- Iterate through each batch of images
  for b=1,numBatches,1 do

    -- read the batch of inputs
    local offset = (b-1)*opt.batchSize
    local batchInputs,batchLabels = readBatchOfImages(list_of_valid_files,list_of_valid_labels,offset,inputSize)

    -- wait for GPU to finish
    cutorch.synchronize()

    -- train the network using the batch of images
    local err_batch,top1_batch = testBatch(batchInputs,batchLabels)
    local acc_top1_batch = top1_batch * 100 / opt.batchSize

    err_epoch = err_epoch + err_batch
    acc_top1_epoch = acc_top1_epoch + top1_batch
  end

  -- Logging the result for each epoch
  acc_top1_epoch = acc_top1_epoch * 100 / (opt.batchSize * numBatches)
  err_epoch = err_epoch / numBatches

  -- TODO: TEST LOGGER ?
--  trainLogger:add{
--      ['% top1 accuracy (train set)'] = acc_top1_epoch,
--      ['avg loss (train set)'] = err_epoch
--  }

  -- printing out the result
  print(string.format('Epoch: [%d][TESTING SUMMARY] \t'
                          .. 'average loss (per batch): %f \t '
                          .. 'accuracy(%%):\t top-1 %f\t',
                       epoch, err_epoch, acc_top1_epoch))
  print('\n')

  -- save model
  collectgarbage()

  net:clearState()

end

-- function to test the neural network with a batch of inputs
function testBatch(batchInputs,batchLabels)
  --cutorch.synchronize()
  --collectgarbage()

  -- convert batch inputs to cuda format
  local batchInputs_cuda = batchInputs:cuda()
  local batchLabels_cuda = batchLabels:cuda()

  local err, top1
  local outputs

  net:zeroGradParameters()

  -- get the outputs
  outputs = net:forward(batchInputs_cuda)

  -- get the output error
  err = criterion:forward(outputs,batchLabels_cuda)

  -- calculate top1 accuracy
  local _,predicted_label = outputs:float():sort(2,true) -- descending order
  top1 = 0
  for i=1,opt.batchSize do
    if predicted_label[i][1] == batchLabels[i] then
      top1 = top1 + 1
    end
  end

  collectgarbage()

  return err,top1
end


experiment()
-------------------- CALLING THE FUNCTION HERE
