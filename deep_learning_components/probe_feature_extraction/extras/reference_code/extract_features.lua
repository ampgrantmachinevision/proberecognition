-- Glimpse of image
require 'torch'
require 'nn'
require 'image'
require 'cutorch'
require 'cunn'
require 'cudnn'
require 'dpnn'

collectgarbage();collectgarbage()

------------------- PRETRAINED MODEL FOR FEATURE EXTRACTION ----------------
--local feature_extractor
local features_per_img
if cutorch.getDeviceCount() > 0 then
  feature_extractor = torch.load('pretrained/resnet-18.t7'):cuda()
else
  feature_extractor = torch.load('pretrained/resnet-18.t7')
end

-- removing the last layer
assert(torch.type(feature_extractor:get(#feature_extractor.modules))=='nn.Linear')
feature_extractor:remove(#feature_extractor.modules)

-- put in evaluate mode
feature_extractor:evaluate()

glimpses_per_img,labels_per_img,num_glimpses_per_img = torch.load('data/glimpses_per_img.t7')
glimpses_per_img_float = glimpses_per_img:float()

print(#glimpses_per_img_float)
local output = feature_extractor:forward(glimpses_per_img_float:cuda()):squeeze(1)

if output:nDimension() == 1 then output = torch.reshape(output, 1, output:size(1)) end
print(#output)

-- STORE THE FEATURES PER IMAGE
features_per_img = torch.FloatTensor(output:size(1),output:size(2))
features_per_img = features_per_img:copy(output)

-- save the glimpses of the image
torch.save('data/features_per_img.t7',features_per_img)
collectgarbage();collectgarbage()

----------------- PRETRAINED MODEL FOR FEATURE EXTRACTION END ---------------