-- Plotting from a saved experiment
require 'dp'
require 'cutorch' -- for cuda libraries
require 'cunn'
require 'cunnx'
require 'torch'
require 'optim'
require 'cudnn'

-- now to use the saved experiment and load figures/graphs of learning and inference.
cmd = torch.CmdLine()
cmd:text()
cmd:text('Save Trained Model from experiment')
cmd:text('Example:')
cmd:text('Options:')
cmd:option('--experiment_name','mvprdeeplearning:1485623471:1','give the name of experiment')
cmd:option('--output_model_file','pretrained/probenet.t7','name of the model')
opt = cmd:parse(arg or {})

-- load the saved experiment and its metadata
experiment_name = opt.experiment_name --'mvprdeeplearning:1484257337:1' -- in future iterations, usd CMD to set experiment
reportPath = os.getenv("HOME") .. '/save/' .. experiment_name .. '/log/'
xpFileName = os.getenv("HOME") .. '/save/' .. experiment_name .. '.dat' -- string concatenation
metaDataFile = reportPath .. 'metadata.dat'


print(xpFileName)

xp = torch.load(xpFileName)
xpMetadata = torch.load(metaDataFile)

net = xp._model
print(net)

-- saving the model
torch.save(opt.output_model_file,net)
print('model saved')
