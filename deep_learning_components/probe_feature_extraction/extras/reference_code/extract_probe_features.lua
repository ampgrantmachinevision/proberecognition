-- New Updated File for obtaining image features
require 'torch'
require 'nn'
require 'image'
require 'cutorch'
require 'cunn'
require 'cudnn'
require 'dpnn'

collectgarbage();collectgarbage()

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Operation of Glimpse module on image:')
cmd:text()
cmd:text('Options:')
cmd:option('-img','data/sample.png','path to where image resides')
cmd:option('-ndepths',2,'the depth of the glimpse') -- controls how many layers are there
cmd:option('-nscales',2,'the number of scales') -- controls how much resolution is reduced at each depth
cmd:option('-glimpse_size',224,'size of the glimpse')
cmd:option('-crop_size',512,'Size of cropped region')

-- options for deep mask
cmd:option('-model','pretrained/deepmask','path to model to load')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 10,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')

local config = cmd:parse(arg)

-------------------DEEP MASK INITIALIZATION------------------------------------------------
-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

--------------------------------------------------------------------------------
-- load model
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.model)
local m = torch.load(config.model..'/model.t7')
local model = m.model
model:inference(config.np)
model:cuda()

--------------------------------------------------------------------------------
-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}

--------------------DEEP MASK END--------------------------------------

-------------------- FEATURE EXTRACTION  -----------------------------------
-- load the image
img = image.load(config.img)
num_depths = config.ndepths;
num_scales = config.nscales;
glimpse_size = config.glimpse_size;
crop_size = config.crop_size;

-- resize image to 768 x 1792 or 512 x 1280

img_segment_base_name = "data/sample_cropped"
img = image.scale(img,768,1792)
--print(#img)

-- obtain glimpses of each of the image
sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)

-- Extract 512 x 512 sub-segments of image and run deep mask to get possible object-centric regions.
total_segments = 0
num_glimpses_per_img = 0;
local glimpses_per_img = torch.FloatTensor(500,img:size(1),glimpse_size,glimpse_size)
local labels_per_img = torch.IntTensor(500,2):zero() -- first column provides labels for probe component, and the second for the configuration

num_segment_rows = img:size(2)/ (crop_size/2) -- 7
num_segment_cols = img:size(3)/ (crop_size/2) -- 3

local total_segments = 0
local num_glimpses_per_img = 0

-- TODO: Create appropriate labels for features of that segment : probe stylus - 1, probe body - 2, probe head - 3 ; Depends on the row index - i = 1,2 : class - 1, i = 3,4 : class - 2; i = 5,6 - class - 3

for i=1,num_segment_rows-1,1 do
  for j=1,num_segment_cols-1,1 do
    h = crop_size
    w = crop_size
    x1 = (j-1)*crop_size/2  -- zero based indexing x
    y1 = (i-1)*crop_size/2 -- zero based indexing y
    x2 = x1 + w
    y2 = y1 + h

    img_crop_level2 = image.crop(img,x1,y1,x2,y2)

    -- Apply deep mask
    infer:forward(img_crop_level2)

    local masks,scores = infer:getTopProps(.2,h,w)
    --print(scores)
    --print(#masks)
    -- save result
    local res = img_crop_level2:clone()

    --Iteratively draw the mask on the image based on the score
    local val_count = 0
    for k=1,config.np,1 do
      score_per_mask = scores[k][1]
      if score_per_mask > 0.5 then
        val_count = val_count + 1
      end
    end

    if val_count > 0 then
      masks_new = masks:sub(1,val_count)
      maskApi.drawMasks(res, masks_new) -- draws only relevant features

      -- encode the mask
      masks_rs = maskApi.encode(masks_new)
      -- get the bounding boxes
      bbs = maskApi.toBbox(masks_rs)

      -- Iterate through each bounding box
      for k = 1,bbs:size(1),1 do
        x = bbs[k][1]
        y = bbs[k][2]
        w = bbs[k][3]
        h = bbs[k][4]

        -- get the segment
        img_seg = image.crop(img_crop_level2,x,y,x+w,y+h)

        -- center location of segment
        loc = torch.Tensor{0,0}

        -- apply the glimpse
        output = sg:forward{img_seg,loc}
        outputs = torch.chunk(output,num_depths)

        for m=1,#outputs,1 do

          --glimpse_img = image.toDisplayTensor(outputs[m])
          glimpse_img = outputs[m]:clone()
         -- print(torch.type(glimpse_img))

          -- convert to FloatTensor from LongStorage

          -- TODO: Preprocess each output, and normalize each channel
          for c = 1,3 do
            glimpse_img[c]:add(-meanstd.mean[c])
            glimpse_img[c]:div(meanstd.std[c])
          end

          num_glimpses_per_img = num_glimpses_per_img + 1
          glimpses_per_img[{num_glimpses_per_img,{},{},{} }] = glimpse_img
          --print(#glimpse_img)

          -- set the label
          -- TODO: This should not be hard-coded!!!
          if i >= 1 and i < num_segment_rows/3 then
            labels_per_img[{num_glimpses_per_img,1}] = 1
          elseif i >= num_segment_rows/3 and i < 2*num_segment_rows/3 then
            labels_per_img[{num_glimpses_per_img,1}] = 2
          elseif i >= 2*num_segment_rows/3 and i < num_segment_rows then
            labels_per_img[{num_glimpses_per_img,1}] = 3
          end
        end

        img_glimpse_crop = image.toDisplayTensor(outputs)

        save_file_name =string.format("%s_%d_%d_glimpse_%d.png",img_segment_base_name,i,j,k)
        --image.save(save_file_name,img_glimpse_crop)
        print(save_file_name)

        total_segments = total_segments+1

      end

    end

    save_file_name =string.format("%s_%d_%d.png",img_segment_base_name,i,j)
    --image.save(save_file_name,res)
    print(save_file_name)

  end
end

mesg_str = string.format("Number of (segments,glimpses) for image %s : (%d,%d)",config.img,total_segments,num_glimpses_per_img)
print(mesg_str)
--print(#glimpses_per_img)

-- truncating to actual size
glimpses_per_img = glimpses_per_img[{ {1,num_glimpses_per_img},{},{},{} }]
labels_per_img = labels_per_img[{ {1,num_glimpses_per_img}, 1 }]
--print(#glimpses_per_img)

------------------- PRETRAINED MODEL FOR FEATURE EXTRACTION ----------------
--local feature_extractor
local features
if cutorch.getDeviceCount() > 0 then
  feature_extractor = torch.load('pretrained/resnet-18.t7'):cuda()
else
  feature_extractor = torch.load('pretrained/resnet-18.t7')
end

-- removing the last layer
assert(torch.type(feature_extractor:get(#feature_extractor.modules))=='nn.Linear')
feature_extractor:remove(#feature_extractor.modules)

-- put in evaluate mode
feature_extractor:evaluate()

glimpses_per_img_float = glimpses_per_img:float()

local output = feature_extractor:forward(glimpses_per_img_float:cuda()):squeeze(1)

-- this is necesary because the model outputs different dimension based on size of input
if output:nDimension() == 1 then output = torch.reshape(output, 1, output:size(1)) end

-- STORE THE FEATURES PER IMAGE
features = torch.FloatTensor(num_glimpses_per_img,output:size(2))
features = features:copy(output)

----------------- PRETRAINED MODEL FOR FEATURE EXTRACTION END ---------------

-- save the glimpses of the image
torch.save('data/features.t7',{features = features,labels_per_img = labels_per_img})
collectgarbage();collectgarbage()