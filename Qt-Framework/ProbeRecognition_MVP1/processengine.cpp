#include "processengine.h"

ProcessEngine::ProcessEngine(int argc, char** argv, QObject *parent) : QObject(parent)
{
    // TODO: parsing arguments and storing them :- NEED TO PARSE THEM PROPERLY
    for(int i = 1; i <= argc; i++)
        app_args.push_back(QString(argv[i]));
}

void ProcessEngine::run(bool flag)
{
    CameraType cam;
    cam.name = "File";
    cam.location = app_args[0]; // NEED TO EDIT THIS
    QString class_file_location = cam.location;
    class_file_location += "/";
    class_file_location += app_args[1];

    // Create the ProbeRecognizer
    this->probe_rec = new ProbeRecognizer();

    // Load the required lua script files
    //this->probe_rec->loadLuaScriptFiles(app_args[1]); // the number of lua files for the interpretor to use

    // load the class file
    this->probe_rec->loadClassLabelFile(class_file_location);

    // opening up stream capture
    QPointer<ImageStreamCapture> im_cap = new ImageStreamCapture(cam);

    // stream capture - from a specific class
    QStringList class_list = this->probe_rec->getSet_of_classes();

    // PREPROCESS LOOP
    for(QStringList::iterator it = class_list.begin(); it!= class_list.end(); it++){
        // get streaming images
        im_cap->getStreamingImages(-1, (*it));

        // retrieve the streaming images
        std::vector<cv::Mat> img_buffer = im_cap->imgbuffer(-1); // retreiving all the images

        // ANOTHER PROCESSING STEP
        // GET INTEREST POINTS ON THE LARGE IMAGE
        // SCAN THE IMAGE USING A TEMPORAL WINDOW
        // WITHIN EACH TEMPORAL WINDOW, PERFORM CLUSTERING OF THE POINTS WITH THE STRONG CLUSTER
        // USE THAT AS LOCATOR POINT.
        // SCAN THE WINDOW IN THE VERTICAL DIRECTOR TO GET A LIST OF LOCATOR POINTS IN SEQUENCE.
        // USE THE GLIMPSE IN LUA TO OBTAIN GLIMPSE VECTORS OF SIZE 9 X 64 X 64.
        // APPLY LSTM NETWORK IN THE SEQUENCE OF AN IMAGE
        // DEEP MASK CAN BE USED ELSEWHERE.

//        // Do some processing - show sample for a single image
//        cv::Mat img = img_buffer[0].clone();
//        std::vector<cv::Mat> imgSegments = this->probe_rec->imageDivision(img);

//        // apply segmentation to those segments
//        for(std::vector<cv::Mat>::iterator it = imgSegments.begin(); it != imgSegments.end(); it++){
//            cv::Mat img = (*it).clone();
//            this->probe_rec->imageSegmentation(img);

//            im_cap->imageDisplay(img,10);
//            //cv::waitKey(10);
//        }



        // Save the streaming images

        // clear the image buffer for that class
        im_cap->clearImgBuffer();
    }

    // DATA STORE/ANNOTATION LOOP

    // TRAINING LOOP

    // TEST RUN LOOP

    // VALIDATION/RESULT COMPUTATION LOOP

    // emit signal that the application is done
    emit this->processingDone();
}

void ProcessEngine::trainTestSplit()
{
    CameraType cam;
    cam.name = "File";
    cam.location = app_args[0]; // NEED TO EDIT THIS
    QString class_file_location = cam.location;
    class_file_location += "/";
    class_file_location += app_args[1];

    // Create the ProbeRecognizer
    this->probe_rec = new ProbeRecognizer();

    // load the class file
    this->probe_rec->loadClassLabelFile(class_file_location);

    QStringList class_list = this->probe_rec->getSet_of_classes();

    // open outputfile to store train test split
    QString train_file_name = cam.location;
    train_file_name += "/";
    train_file_name += "train.txt";
    QFile train_file(train_file_name);
    train_file.open(QIODevice::WriteOnly);
    QTextStream train_out_file(&train_file);

    QString test_file_name = cam.location;
    test_file_name += "/";
    test_file_name += "test.txt";
    QFile test_file(test_file_name);
    test_file.open(QIODevice::WriteOnly);
    QTextStream test_out_file(&test_file);

    QString all_file_name = cam.location;
    all_file_name += "/";
    all_file_name += "files.txt";
    QFile all_file(all_file_name);
    all_file.open(QIODevice::WriteOnly);
    QTextStream all_out_file(&all_file);


    // some change

    // Read the image files
    int class_count = 1;
    std::vector<QString> abs_list_of_image_files;
    for(QStringList::iterator it = class_list.begin(); it!= class_list.end(); it++){
        QString class_name = *it;
        QString img_location = cam.location;
        img_location += "/";
        img_location += class_name;

        // setting the filters and name filters for retreiving images
        QDir dir(img_location);
        //std::cout << img_location.toStdString() << std::endl;
        QStringList filters;
        filters << "*.jpg" << "*.png" << "*.tiff";
        dir.setNameFilters(filters);
        //dir.setFilter(QDir::Files);

        QStringList imgList = dir.entryList();

        // TODO: create the train/test split for each class and populate a single train.txt and test.txt file
        // filename class(index format)
        // This will be read by torch, and generate appropriate features, or provide incentive to train an additional network.

        int count = 0;
        for(QStringList::iterator it = imgList.begin(); it!= imgList.end(); ++it){
            QString img_path_absolute = img_location + "/" + (*it);
            img_path_absolute += "\t";
            img_path_absolute += QString::number(class_count);
            img_path_absolute += "\n";
            //std::string img_name = img_path_absolute.toStdString();
            abs_list_of_image_files.push_back(img_path_absolute);
        }
        class_count++;
    }

    // Reorder/Shuffle filenames
    // create a random ordering of integers

    // populate QMap class with random ordering of integers and classes
    // print the QMap in order into a text file
    std::srand ( unsigned ( std::time(0) ) );
    std::random_shuffle(abs_list_of_image_files.begin(),abs_list_of_image_files.end());
    int num_img_files = 0;
    int offset = 1;
    int count = 0;

    // take 10% of actuall data
    for(std::vector<QString>::iterator it = abs_list_of_image_files.begin(); it!= abs_list_of_image_files.end(); ++it){
        QString img_path_absolute = (*it);
        std::string img_name = img_path_absolute.toStdString();
        //abs_list_of_image_files.push_back(QString(img_name));
        if(num_img_files%offset == 0){
            all_out_file << img_path_absolute; //<< "\t" << class_count << "\n";
            std::cout << img_path_absolute.toStdString() << std::endl;
            count++;
        }
        num_img_files++;
    }

    int train_size = std::floor(0.8 * num_img_files);
    int valid_size = std::floor(0.1 * num_img_files);
    int test_size = num_img_files - train_size - valid_size;

    // take 90% for training, 10% testing -  all images
    for(std::vector<QString>::iterator it = abs_list_of_image_files.begin(); it!= abs_list_of_image_files.begin()+train_size; ++it){
        QString img_path_absolute = (*it);
        std::string img_name = img_path_absolute.toStdString();
        //abs_list_of_image_files.push_back(QString(img_name));
        train_out_file << img_path_absolute; //<< "\t" << class_count << "\n";
    }

    // all images
    for(std::vector<QString>::iterator it = abs_list_of_image_files.begin()+train_size; it!= abs_list_of_image_files.end(); ++it){
        QString img_path_absolute = (*it);
        std::string img_name = img_path_absolute.toStdString();
        //abs_list_of_image_files.push_back(QString(img_name));
        test_out_file << img_path_absolute; //<< "\t" << class_count << "\n";
    }

    std::cout << "train test split captured " << std::endl;


    // emit signal that the application is done
    emit this->processingDone();

}
