-- Glimpse of image
require 'torch'
require 'cutorch'
require 'cudnn'
require 'dpnn'
require 'nn'
require 'image'

collectgarbage();collectgarbage()

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Operation of Glimpse module on image:')
cmd:text()
cmd:text('Options:')
cmd:option('-img','data/sample.png','path to where image resides')
cmd:option('-ndepths',2,'the depth of the glimpse') -- controls how many layers are there
cmd:option('-nscales',2,'the number of scales') -- controls how much resolution is reduced at each depth
cmd:option('-glimpse_size',224,'size of the glimpse')
cmd:option('-crop_size',512,'Size of cropped region')

-- options for deep mask
cmd:option('-model','pretrained/deepmask','path to model to load')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 10,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')

local config = cmd:parse(arg)

-------------------DEEP MASK INITIALIZATION------------------------------------------------
-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

--------------------------------------------------------------------------------
-- load model
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.model)
local m = torch.load(config.model..'/model.t7')
local model = m.model
model:inference(config.np)
model:cuda()

--------------------------------------------------------------------------------
-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}

--------------------DEEP MASK END--------------------------------------

-------------------- FEATURE EXTRACTION  -----------------------------------
-- load the image
img = image.load(config.img)
num_depths = config.ndepths;
num_scales = config.nscales;
glimpse_size = config.glimpse_size;
crop_size = config.crop_size;

-- resize image to 768 x 1792 or 512 x 1280

-- preprocess image
segment_list = {"c","tl","tr","bl","br"}

img_segment_base_name = "data/sample_cropped"
img = image.scale(img,768,1792)
print(#img)

-- obtain glimpses of each of the image
sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)

-- TODO: Dont do 256 x 256 - do 512 x 512
-- create 256 x 256 cropped regions by cropping each image as a hierarchy
total_segments = 0
num_glimpses_per_img = 0;
local glimpses_per_img = torch.FloatTensor(100,img:size(1),glimpse_size,glimpse_size)
for i=1,#segment_list,1 do
  local img_crop_level1 = image.crop(img,segment_list[i],crop_size,crop_size)
  --for j=1,#segment_list,1 do
  -- local img_crop_level2 = image.crop(img_crop_level1,segment_list[j],crop_size/2,crop_size/2)
    j = 1
    img_crop_level2 = img_crop_level1:clone()
    c_size = crop_size -- divide by 2 if using 256 x 256
    -- now do segmentation on the img_crop_level2
    ind = (i-1) * (#segment_list) + j

    -- apply deep mask
    infer:forward(img_crop_level2)

      -- get top propsals
    h,w = c_size, c_size

    local masks,scores = infer:getTopProps(.2,h,w)
    print(scores)
    print(#masks)
    -- save result
    local res = img_crop_level2:clone()

    --Iteratively draw the mask on the image based on the score
    local val_count = 0
    for k=1,config.np,1 do
      score_per_mask = scores[k][1]
      if score_per_mask > 0.5 then
        val_count = val_count + 1
      end
    end

    if val_count > 0 then
      masks_new = masks:sub(1,val_count)
      maskApi.drawMasks(res, masks_new) -- draws only relevant features

      -- encode the mask
      masks_rs = maskApi.encode(masks_new)
      -- get the bounding boxes
      bbs = maskApi.toBbox(masks_rs)

      -- TODO: For each bounding box, extract the segments
      -- Iterate through each bounding box
      for k = 1,bbs:size(1),1 do
        x = bbs[k][1]
        y = bbs[k][2]
        w = bbs[k][3]
        h = bbs[k][4]

        -- get the segment
        img_seg = image.crop(img_crop_level2,x,y,x+w,y+h)

        -- TODO: maybe instead of segment, use the location point

        -- center location of segment
        loc = torch.Tensor{0,0}

        -- apply the glimpse
        output = sg:forward{img_seg,loc}

        print('Direct output of Glimpse network')
        print(#output)

        outputs = torch.chunk(output,num_depths)
        print('Output after chunking')
        print(#outputs[1])

        for m=1,#outputs,1 do

          --glimpse_img = image.toDisplayTensor(outputs[m])
          glimpse_img = outputs[m]:clone()
          print(torch.type(glimpse_img))

          -- convert to FloatTensor from LongStorage

        -- TODO: Preprocess each output, and normalize each channel
          for c = 1,3 do
            glimpse_img[c]:add(-meanstd.mean[c])
            glimpse_img[c]:div(meanstd.std[c])
          end

          num_glimpses_per_img = num_glimpses_per_img + 1
          glimpses_per_img[{num_glimpses_per_img,{},{},{} }] = glimpse_img
          --print(#glimpse_img)

        end
        -- TODO: The descriptors at the outputs of each channel will concatenated or made to go through inception layer
        -- TODO: Get the concantencated features for the segment.
        -- TODO: save the segment feature descriptor and annotate as channel 'c'.

        img_glimpse_crop = image.toDisplayTensor(outputs)

        save_file_name =string.format("%s_%s_%s_glimpse_%d.png",img_segment_base_name,segment_list[i],segment_list[j],k)
        image.save(save_file_name,img_glimpse_crop)

        total_segments = total_segments+1

      end

      save_file_name =string.format("%s_%s_%s.png",img_segment_base_name,segment_list[i],segment_list[j])
      image.save(save_file_name,res)

    end
    collectgarbage()

  --end
end
mesg_str = string.format("Number of (segments,glimpses) for image %s : (%d,%d)",config.img,total_segments,num_glimpses_per_img)
print(mesg_str)
print(#glimpses_per_img)

-- truncating to actual size
glimpses_per_img = glimpses_per_img[{ {1,num_glimpses_per_img},{},{},{} }]
print(#glimpses_per_img)

-- save the glimpses of the image
torch.save('data/glimpses_per_img.t7',glimpses_per_img)
collectgarbage();collectgarbage()
-- TODO: Accumulate segment descriptors across all channels for a single image.

-- TODO: Extract features


-- TODO: Compute Bag of Words model

-- TODO: Compute histogram of codewords

-- TODO: Set weights to each codeword(optional)

-- TODO: Train fully-connected layer to recognize probes
---------------------------- GLIMPSE MODULE END -------------------------------

