#ifndef PROBERECOGNIZER_H
#define PROBERECOGNIZER_H

#include <QObject>
#include <QProcess>
#include <QPointer>
#include <opencv2/opencv.hpp>

#include <QFile>
#include <QTextStream>

#include "dnnfeatureextractor.h"
#include "dnnclassifier.h"
#include "imagestreamcapture.h"

//#include "qtlua/qtluaengine.h"

class ProbeRecognizer : public QObject
{
    Q_OBJECT
public:
    explicit ProbeRecognizer(QObject *parent = 0);

// model learning/inference methods
//    void loadModel();
//    void testSingleImage();
//    void testBatchImages();
//    void generateReport();
//    bool setShowImageFlag();

    // loading script files
    void loadLuaScriptFiles(QString file);
    void loadClassLabelFile(QString file);

    // preprocessing
    void preprocess(cv::Mat& img);
    void classify(cv::Mat& img);
    void createTrainingDataLabels(cv::Mat& img);
    std::vector<cv::KeyPoint> featureDetector(cv::Mat& img);

    QStringList getSet_of_classes() const;

    void imageSegmentation(cv::Mat& img);
    std::vector<cv::Mat> imageDivision(cv::Mat& img);

signals:

public slots:

protected:

    // methods which only the class and its derivatives will use
    QPointer<ImageStreamCapture> im_cap;

    // Instantiate Qt Lua Engine/Interpreter
    QStringList lua_script_files;
    QStringList set_of_classes;

};

#endif // PROBERECOGNIZER_H
