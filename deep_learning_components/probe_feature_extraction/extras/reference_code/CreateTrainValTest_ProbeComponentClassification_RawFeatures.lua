-- Creationg of Data source from the extracted features using DP method.
require 'torch'
require 'torchx'
require 'dp'

cmd = torch.CmdLine()

-- listing the various options
cmd:option('-features','data/features-all.t7','path containing the features for training')
cmd:option('-labels','data/component_labels-all.t7','path containing the labels for training')
cmd:option('-validRatio',0.2,'Percentage of samples to be used for validation')
cmd:option('-testRatio',0.2,'Percentage of samples to be used for testing')
cmd:option('-dataSourceFile','data/ProbeComponentDataSource.t7','Name of the datasource')
cmd:option('-setOfClasses','data/probe_component_classes.txt','File containing the names of the classes')

-- parse the options
config = cmd:parse(arg)

-- load the class file
classes_file = torch.DiskFile(config.setOfClasses,"r")
classes_file:quiet()
num = 1
set_of_classes = {}
repeat
  classes_str = classes_file:readString("*l")
  set_of_classes[num] = classes_str
  num = num+1
until classes_str == ''
set_of_classes[num-1] = nil

print('Set of classes:')
print(set_of_classes)

-- load the features
features = torch.load(config.features)
labels = torch.load(config.labels)

local num_samples = 0
local num_dims = features[1]:size(2)

-- Iterate through samples
for i, v in ipairs(features) do
  num_samples = num_samples + v:size(1)
end

-- combine table of features into 1 Tensor
local input = torch.FloatTensor(num_samples,num_dims)
local target = torch.IntTensor(num_samples)
local s_input = torch.FloatTensor(num_samples,num_dims)
local s_target = torch.IntTensor(num_samples)

-- iterate again through samples to populate input and target
cum_idx = 1
for i,v in ipairs(features) do
  local labels_per_img = labels[i]
  local features_per_img = v:clone()
  local num_samples_per_img = v:size(1)

  input[{ {cum_idx,cum_idx+num_samples_per_img-1},{1,num_dims} }] = v
  target[{ {cum_idx,cum_idx+num_samples_per_img-1}}] = labels_per_img

  cum_idx = cum_idx + num_samples_per_img

  collectgarbage()
end

print(#input)
print(#target)

-- create a shuffled index
shuffle = torch.randperm(num_samples)

-- shuffle the inputs and target
--for i = 1,1,num_samples do
--  idx = shuffle[i]
--  s_input[{ {idx}, {} }]:copy(input[i])
--  s_target[{idx}]:copy(target[i])
--end

s_input = input:clone()
s_target = target:clone()

print(#s_input)
print(#s_target)

-- TODO: DIVIDE INTO TRAIN, VALIDATION AND TEST SET
local nValid = math.floor(num_samples*config.validRatio)
local nTest = math.floor(num_samples*config.testRatio)
local nTrain = num_samples - nValid - nTest

local trainInput = dp.DataView('bf',s_input:narrow(1,1,nTrain))
local trainTarget = dp.ClassView('b',s_target:narrow(1,1,nTrain))

local validInput = dp.DataView('bf',s_input:narrow(1,nTrain+1,nValid))
local validTarget = dp.ClassView('b',s_target:narrow(1,nTrain+1,nValid))

local testInput = dp.DataView('bf',s_input:narrow(1,nTrain+nValid+1,nTest))
local testTarget = dp.ClassView('b',s_target:narrow(1,nTrain+nValid+1,nTest))

trainTarget:setClasses(set_of_classes)
validTarget:setClasses(set_of_classes)
testTarget:setClasses(set_of_classes)

-- wrap into dataset views
local train = dp.DataSet{inputs=trainInput, targets = trainTarget, which_set='train'}
local valid = dp.DataSet{inputs=validInput, targets = validTarget, which_set='valid'}
local test = dp.DataSet{inputs=testInput, targets = testTarget, which_set='test'}

local input_preprocess = {}
table.insert(input_preprocess, dp.Standardize())

local ds = dp.DataSource{train_set = train, valid_set = valid, test_set=test,input_preprocess = input_preprocess}
ds:classes(set_of_classes)

print(ds)

torch.save(config.dataSourceFile,ds)

print('Operation completed successfully')
print('Printing out sample data')
tensor_train = ds:get('train', 'input', 'default')
tensor_test = ds:get('test', 'input', 'default')
tensor_valid = ds:get('valid','input','default')

print(#tensor_train)
print(#tensor_test)
print(#tensor_valid)