#include <QCoreApplication>
#include <processengine.h>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    ProcessEngine pr(argc,argv);

    // connection to slot
    QObject::connect(&pr,&ProcessEngine::processingDone, &app, &QCoreApplication::quit,Qt::QueuedConnection);

    // start running the process
    //pr.run(); // this method will call the slot for the app to exit
    pr.trainTestSplit();

    return app.exec();
}
