#ifndef BASECLASSIFIER_H
#define BASECLASSIFIER_H

#include <QObject>
#include <vector>
#include <opencv2/opencv.hpp>

class BaseClassifier : public QObject
{
    Q_OBJECT
public:
    explicit BaseClassifier(QObject *parent = 0);
//    virtual void trainModel(); // make it virtual later
//    virtual void inference(cv::Mat);

//    void setSamples(std::vector<cv::Mat> entries);

signals:

public slots:

protected:
    int m_num_samples;
    std::vector<cv::Mat> m_entries;
};

#endif // BASECLASSIFIER_H
