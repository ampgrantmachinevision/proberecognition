-- Organize images from files.txt into train, test and valid folders
require 'torch'
require 'paths'
require 'torchx'
require 'image'

local cmd = torch.CmdLine()

cmd:text('Organization of train/test folders')

cmd:option('-allFile','data/files_ProbeSegments4.txt','path to where the files are stored')
cmd:option('-trainFile','data/train_ProbeSegments4.txt','path to where the files are stored')
cmd:option('-testFile','data/test_ProbeSegments4.txt','path to where the files are stored')
cmd:option('-trainTestSplit',true,'flag to check if we can use given train test split')

cmd:option('-dataPath','data/ProbeSegments4_WithTrainTestSplit','path where the images are stored')
cmd:option('-setOfClasses','data/probe_component_classes_specific.txt','File containing the names of the classes')
cmd:option('-validTestRatio',0.2,'ratio for valid and test data')

local config = cmd:parse(arg)

-- Load the probe component class file
classes_file = torch.DiskFile(config.setOfClasses,"r")
classes_file:quiet()
num = 1
set_of_classes = {}
repeat
  classes_str = classes_file:readString("*l")
  set_of_classes[num] = classes_str
  num = num+1
until classes_str == ''
set_of_classes[num-1] = nil

-- create a directory structure such as images/train/[class-id], images/valid/[class-id], images/test/[class-id]
if paths.mkdir(config.dataPath) then print('image directory created'); else print('image directory already present') end

train_dir = config.dataPath .. '/train';
if paths.mkdir(train_dir) then print('train directory created'); else print('train directory already present') end
valid_dir = config.dataPath .. '/valid'; paths.mkdir(valid_dir)
if paths.mkdir(valid_dir) then print('valid directory created'); else print('valid directory already present') end
test_dir = config.dataPath .. '/test'; paths.mkdir(test_dir)
if paths.mkdir(test_dir) then print('test directory created'); else print('test directory already present') end

for i,v in ipairs(set_of_classes) do
    train_class_path = string.format('%s/%d',train_dir,i); paths.mkdir(train_class_path)
    if paths.mkdir(train_class_path) then print('train class directory created'); else print('train class directory already present') end

    valid_class_path = string.format('%s/%d',valid_dir,i); paths.mkdir(valid_class_path)
    if paths.mkdir(valid_class_path) then print('valid class directory created'); else print('valid class directory already present') end

    test_class_path = string.format('%s/%d',test_dir,i); paths.mkdir(test_class_path)
    if paths.mkdir(test_class_path) then print('test class directory created'); else print('test class directory already present') end
end

local all_file, train_file, test_file
local num_samples, num_valid_samples, num_test_samples, num_train_samples
local count = 1
local img_files = {}
local img_class_labels = {}

if(config.trainTestSplit) then
-- Read all files
  train_file = io.open(config.trainFile)
  test_file = io.open(config.testFile)

  -- read all the train file names
  while true do
    local line = train_file:read()
    if line == nil then
      break
    end
    --print(line)
    local k = string.find(line,'\t')
    local img_file_name = string.sub(line,1,k-1)
    local img_class = string.sub(line,k+1,-1)

    img_files[count] = img_file_name
    img_class_labels[count] = img_class

    count = count + 1
  end
  num_train_samples = #img_files

  -- read all the test files
  while true do
    local line = test_file:read()
    if line == nil then
      break
    end
    --print(line)
    local k = string.find(line,'\t')
    local img_file_name = string.sub(line,1,k-1)
    local img_class = string.sub(line,k+1,-1)

    img_files[count] = img_file_name
    img_class_labels[count] = img_class

    count = count + 1
  end

  train_file:close(train_file)
  test_file:close(test_file)

  num_samples = #img_files
  num_test_samples = num_samples - num_train_samples
  num_valid_samples = torch.floor(config.validTestRatio * num_samples)
  num_train_samples = num_train_samples - num_valid_samples

else
  -- Read all files
  all_file = io.open(config.allFile)
  -- read all the train file names

  while true do
    local line = all_file:read()
    if line == nil then
      break
    end
    --print(line)
    local k = string.find(line,'\t')
    local img_file_name = string.sub(line,1,k-1)
    local img_class = string.sub(line,k+1,-1)

    img_files[count] = img_file_name
    img_class_labels[count] = img_class

    count = count + 1
  end
  all_file:close(all_file)


  num_samples = #img_files
  num_valid_samples = torch.floor(config.validTestRatio * num_samples)
  num_test_samples = torch.floor(config.validTestRatio * num_samples)
  num_train_samples = num_samples - num_valid_samples - num_test_samples
end

print('Total number of samples : ' .. num_samples)
print('Number of train samples: ' .. num_train_samples)
print('Number of valid samples: '.. num_valid_samples)
print('Number of test samples: ' .. num_test_samples)

count = 0

-- save the total number of samples
tbl_num_samples = {}
tbl_num_samples['numTrain'] = num_train_samples
tbl_num_samples['numValid'] = num_valid_samples
tbl_num_samples['numTest'] = num_test_samples

torch.save('data/images_2/meta/numSamples.th7',tbl_num_samples)
-- saving train, valid and test images
for i,v in ipairs(img_files) do
  local img = image.load(v)
  local img_label = img_class_labels[i]
  local img_filename
  if i < 10 then img_filename = string.format('imagenum-000000%d.png',i); end
  if i >= 10 and i < 100 then img_filename = string.format('imagenum-00000%d.png',i); end
  if i >= 100 and i < 1000 then img_filename = string.format('imagenum-0000%d.png',i); end
  if i >= 1000 and i < 10000 then img_filename = string.format('imagenum-000%d.png',i); end
  if i >= 10000 and i < 100000 then img_filename = string.format('imagenum-00%d.png',i); end
  if i >= 100000 and i < 1000000 then img_filename = string.format('imagenum-0%d.png',i); end
  if i >= 1000000 then img_filename = string.format('imagenum-%d.png',i); end

  if i <= num_train_samples  then
    img_filename_abs = string.format('%s/%s/%s',train_dir,img_label,img_filename)
  elseif i > num_train_samples and i <= (num_train_samples+num_valid_samples) then
    img_filename_abs = string.format('%s/%d/%s',valid_dir,img_label,img_filename)
  else
    img_filename_abs = string.format('%s/%d/%s',test_dir,img_label,img_filename)
  end

  if i % 500 == 0 then
    print(img_filename_abs)
  end
  image.save(img_filename_abs,img)

end
