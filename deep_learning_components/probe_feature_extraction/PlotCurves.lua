-- Plotting from a saved experiment
require 'dp'
require 'cutorch' -- for cuda libraries
require 'cunn'
require 'cunnx'
require 'torch'
require 'optim'
require 'cudnn'

-- now to use the saved experiment and load figures/graphs of learning and inference.
cmd = torch.CmdLine()
cmd:text()
cmd:text('Plot Channel Curves')
cmd:text('Example:')
cmd:text('$> th plotcurves.lua --entry 1637 --channel "optimizer:feedback:perplexity:ppl,validator:feedback:perplexity:ppl,tester:feedback:perplexity:ppl" --curveName "train,valid,test" -x "epoch", -y "ppl"')
cmd:text('Options:')
cmd:option('--entry', -1, 'comma separated xplog entry number')
cmd:option('--collection', -1, 'comma separated xplog collection id')
cmd:option('--channel', 'optimizer:feedback:confusion:accuracy,validator:feedback:confusion:accuracy,tester:feedback:confusion:accuracy', 'comma-seperated channel names')
cmd:option('--name', 'train,valid,test', 'comma-seperated curve names')
cmd:option('-x', 'epoch', 'name of the x-axis')
cmd:option('-y', 'accuracy', 'name of the y-axis')
cmd:option('--minima', false, 'plot learning curves and include minima')
cmd:option('--experiment_name','mvprdeeplearning:1485623471:1','give the name of experiment')
cmd:text()
opt = cmd:parse(arg or {})

-- just plot the loss
--opt.channel = "optimizer:loss,validator:loss,tester:loss"

-- load the saved experiment and its metadata
experiment_name = opt.experiment_name  --'mvprdeeplearning:1484257337:1' -- in future iterations, usd CMD to set experiment
reportPath = os.getenv("HOME") .. '/save/' .. experiment_name .. '/log/'
xpFileName = os.getenv("HOME") .. '/save/' .. experiment_name .. '.dat' -- string concatenation
accFigureName = os.getenv("HOME") .. '/save/' .. experiment_name .. '_' .. 'accuracy.png'
errFigureName = os.getenv("HOME") .. '/save/' .. experiment_name .. '_' .. 'error.png'
networkTextFile = os.getenv("HOME") .. '/save/' .. experiment_name .. '_' .. 'network_file.txt'
metaDataFile = reportPath .. 'metadata.dat'

print(xpFileName)


xp = torch.load(xpFileName)
xpMetadata = torch.load(metaDataFile)

--string.format(xp._model)
--io.output(networkTextFile)
--io.write(xp._model)
--io.close()
--serializedString = torch.serialize(xp._model)
--print(serializedString)

net = xp._model
print(net)
fd = io.open(networkTextFile,"w")

--for i,module in ipairs(net:listModules()) do
--   -- local net_string = string.format('%s - ',torch.type(module))
--   --fd:write(table:string(xp._model))
--   local weight = module:parameters()
--   print(torch.type(module))
--   print(weight)

--end

--local json = require('json')
--local serializedString = json.encode(xp._model)
--network_file = torch.DiskFile(networkTextFile,"w")
--network_file:writeString(serializedString)
--network_file:close()
--
--
fd:close()

-- in this experiment, the metadata consists of the number of iterations for this experiment
numEpochs = xpMetadata

-- TODO: Print the network configuration


-- read each report from the experiment's log
xpReports = {}
for i = 1,numEpochs do
    local reportFileName = reportPath .. 'report_' .. i .. '.dat'
    local report = torch.load(reportFileName)

    xpReports[i] = report
    --print(report)

    local mesg = 'Report ' .. i .. ' for experiment ' .. experiment_name .. ' loaded'
    print(mesg)
end

-- for plotting directly from reports generated from experiment
-- not using 'dp' package, not sure how this done.
-- create the XPLogEntry and load the loaded reports of the experiment.
xpId = xp:id()
xpId_str = xpId:toString()
xpLogEntry = dp.XpLogEntry{id = xpId_str}
xpLogEntry._reports = xpReports

--print(xpLogEntry:reports())

--if(type(opt.channel) == 'string') then
--    opt.channel = _.split(opt.channel,',')
--end

--if(type(opt.name) == 'string') then
--    opt.name = _.split(opt.name,',')
--end

--print(opt.channel[1])

--print(opt.name[1])

--curve_names = opt.name or opt.channel
--print(curve_names)

--local reports = xpLogEntry:reports()
--local x = torch.Tensor(_.keys(reports))
--local curves = {}
--for i,channel in ipairs(opt.channel) do
--    local values = xpLogEntry:reportChannel(channel, reports)
--    table.insert(
--        curves, { curve_names[i], x, torch.Tensor(values), '-' }
--    )
--end
--require 'gnuplot'
print(opt.x)
print(opt.y)
print(opt.name)

require 'gnuplot'
gnuplot.pngfigure(accFigureName)
xpLogEntry:plotReportChannel{channels=opt.channel, curve_names = opt.name, x_name=opt.x, y_name=opt.y}
gnuplot.xlabel(opt.x)
gnuplot.ylabel(opt.y)

opt.channel = "optimizer:loss,validator:loss,tester:loss"
opt.y = "Error rate"

gnuplot.pngfigure(errFigureName)
xpLogEntry:plotReportChannel{channels=opt.channel, curve_names = opt.name, x_name=opt.x, y_name=opt.y}
gnuplot.xlabel(opt.x)
gnuplot.ylabel(opt.y)
---- printing one report
--report = xpReports[1]
--print(report)