-- Probe Net
------------------------------ NETWORK DEFINITION ---------------------------
-- define the Fully connected network
-- TODO: Define the network here, but later define in another file which can be read.
-- code reference : https://github.com/facebook/fb.resnet.torch/blob/master/models/resnet.lua
local function basicResidualBlock(nIn,nOut,stride)
  -- convolution branch
  local resBlock_br1 = nn.Sequential()

  -- adding the layer
  resBlock_br1:add(nn.SpatialConvolution(nIn,nOut,3,3,stride,stride,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))
  resBlock_br1:add(nn.ReLU())
  resBlock_br1:add(nn.SpatialConvolution(nOut,nOut,3,3,1,1,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))

  -- indentity branch

  -- Create a layer to house the two branches
  local resBlock = nn.ConcatTable()
  resBlock:add(resBlock_br1)
  if nIn ~= nOut then
    resBlock:add(nn.SpatialConvolution(nIn,nOut,1,1,stride,stride))
    --resBlock_br2:add(cudnn.SpatialBatchNormalization(nOut))
  else
    resBlock:add(nn.Identity())
  end

  local finalResBlock = nn.Sequential()
  finalResBlock:add(resBlock)
  finalResBlock:add(nn.CAddTable(true))
  finalResBlock:add(nn.ReLU(true))

  return finalResBlock
end

function create_model(config)
  -- define local functions which define the residual layer (basic block
  local net = torch.load('pretrained/resnet-18.t7')
  numChannels = config.load_size[1]
  numRows = config.load_size[2]
  numCols = config.load_size[3]
  numBatches = 5

  net:remove(#net.modules) -- remove the last fully connected layer (1000-d)
  net:remove(#net.modules) -- remove the 512 layer
  net:remove(#net.modules) -- remove the SpatialAveragePooling layer

--  -- remove the last two residual layers which models block by block relations (conv#4 and conv#5)
  net:remove(#net.modules) -- remove the 4th convolution layer -- output is 256 @ 14 x 14
--  net:remove(#net.modules)

--  -- define 4th conv residual layers
--  net:add(basicResidualBlock(128,256,2))
--  net:add(basicResidualBlock(256,256,1))

  -- insert a rescaling to scale image to 224 x 224
  net:insert(nn.SpatialUpSamplingBilinear({oheight=224, owidth=224}),1)

   -- inefficient way of converting to cuda at every juncture where any modification takes place
  cudnn.convert(net, cudnn)
  net = net:cuda()

  -- create sample image
  local sampleData = torch.Tensor(numBatches,numChannels,numRows,numCols)

  numInputsFCC = #net:forward(sampleData:cuda())

--  -- define the 5th conv residual layer
  net:add(basicResidualBlock(numInputsFCC[2],numInputsFCC[2]*2,2))
--  net:add(basicResidualBlock(512,512,1))

--  -- add the spatial average pooling filter
  net:add(nn.SpatialAveragePooling(7,7,1,1)) -- hard coded 7 x7 filter
  net:add(nn.View(-1):setNumInputDims(3))

   -- inefficient way of converting to cuda at every juncture where any modification takes place
  cudnn.convert(net, cudnn)
  net = net:cuda()

  numInputsFCC = #net:forward(sampleData:cuda())

  -- fully connected layer
  net:add(nn.Linear(numInputsFCC[2],1000))
  net:add(nn.ReLU())
  net:add(nn.Linear(1000,config.outputSize))
  net:add(nn.LogSoftMax())

   -- inefficient way of converting to cuda at every juncture where any modification takes place
  cudnn.convert(net, cudnn)
  net = net:cuda()

  return net

end