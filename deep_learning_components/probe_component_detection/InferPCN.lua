require 'dp'
require 'cutorch' -- for cuda libraries
require 'cunn'
require 'cunnx'
require 'torch'
require 'optim'
require 'cudnn'
require 'image'

local json = require( "json" )

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Probe Component Network to classify various patches in an image and obtain confidence scores')

-- options for pcn
cmd:option('-pcn_model','trained_models/probenet_2.t7','Trained model which takes an input of size 128 x 128 and outputs a glimpse score')
cmd:option('-meanstd','trained_models/meanstd.th7','mean and standard deviation of files')
cmd:option('-meta_path','/meta','path containing the metadata such as mean and standard deviation')
cmd:option('-image_file','data/sample_3.png','CMM probe image to be evaluated')
cmd:option('-ndepths',3,'the depth of the glimpse') -- controls how many layers are there
cmd:option('-nscales',3,'the number of scales') -- controls how much resolution is reduced at each depth ; -- latest values are 3; for now putting the earlier value of 3
cmd:option('-glimpse_size',64,'size of the glimpse') --  latest value is 128 ; setting the earlier value as 64
cmd:option('-crop_size',512,'Size of cropped region')
cmd:option('-setOfClasses','data/probe_component_classes_specific.txt','File containing the names of the classes')
cmd:option('-resultFile','data/results.json','JSON file which contains the results')

-- options for deep mask
cmd:option('-model','pretrained/deepmask','path to model to load')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 10,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')

local config = cmd:parse(arg or{})

-- Load the probe component class file
classes_file = torch.DiskFile(config.setOfClasses,"r")
classes_file:quiet()
num = 1
set_of_classes = {}
repeat
  classes_str = classes_file:readString("*l")
  set_of_classes[num] = classes_str
  num = num+1
until classes_str == ''
set_of_classes[num-1] = nil
print(set_of_classes)

------------------- INITIALIZATION DEEP MASK AND GLIMPSE MODULE ----------------
-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

-- load deep mask model
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.model)
local m = torch.load(config.model..'/model.t7')
local model = m.model
model:inference(config.np)
model:cuda()

-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}

local num_depths = config.ndepths;
local num_scales = config.nscales;
local glimpse_size = config.glimpse_size;
local crop_size = config.crop_size;

local sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)

-------------------------------------------------------------------------------

----------------------- GENERATION OF GLIMPSES -------------------------------

-- load image
local img = image.load(config.image_file)
img = image.scale(img,768,1792)

-- load the model and metadata
local pcn_model = torch.load(config.pcn_model)
pcn_model = pcn_model:cuda()
pcn_model:add(nn.SoftMax():cuda())
pcn_model:evaluate()


local meanstd_probe = torch.load(config.meanstd)
--print(meanstd_probe)

probe_mean = torch.Tensor(meanstd_probe.mean)
probe_std = torch.Tensor(meanstd_probe.std)
--print(probe_mean)
--print(probe_std)
--local scores_image = generate_confidence_scores(img,pcn_model)
--print(scores_image)

-- function to generate glimpses from an image

local scores = {}
local num_segment_rows = img:size(2)/ (crop_size/2) -- 7
local num_segment_cols = img:size(3)/ (crop_size/2) -- 3
local segment_count = 0

-- Function to normalize
local function normalizeImg(img,mean,std)
  num_c = img:size(1)
  for i=1,num_c,1 do
    img[i]:add(-mean[i])
    img[i]:div(std[i])
  end
  return img
end

class_count = torch.Tensor(#set_of_classes):zero()
for i=1,num_segment_rows-1,1 do
  for j=1,num_segment_cols-1,1 do
    w = crop_size
    h = crop_size
    x1 = (j-1)*crop_size/2; y1 = (i-1)*crop_size/2
    x2 = x1 + w ; y2 = y1 + h

    -- cropping the image : img_crop is of size crop_size x crop_size
    local img_crop = image.crop(img,x1,y1,x2,y2)
    segment_count = segment_count + 1
    -- Apply deep mask
    infer:forward(img)

    local masks,scores = infer:getTopProps(.2,h,w)
    local res = img_crop:clone()

    -- filter the masks based on score
    local val_count = 0
    for k=1,config.np,1 do
      score_per_mask = scores[k][1]
      if score_per_mask > 0.2 then
        val_count = val_count + 1
      end
    end

    collectgarbage()

    -- only if any masks remain
    if val_count > 0 then
      masks_new = masks:sub(1,val_count)
      --maskApi.drawMasks(res, masks_new)

      -- encode the mask
      masks_rs = maskApi.encode(masks_new)
      -- get the bounding boxes
      bbs = maskApi.toBbox(masks_rs)

      -- Iterate through each bounding box
      for k = 1,bbs:size(1),1 do
        -- Applying method 1 ( refer extract_glimpses_all_images.lua)
        x = bbs[k][1] ; y = bbs[k][2] ; w = bbs[k][3] ; h = bbs[k][4]

        cent_x = x + w/2
        cent_y = y + h/2

        new_x = math.max(x1 + cent_x -255,1)
        new_y = math.max(y1 + cent_y - 255,1)
        new_x2 = math.min(x1 + cent_x + 256,img:size(3))
        new_y2 = math.min(y1 + cent_y + 256,img:size(2))

        -- get the segment
        local img_seg = image.crop(img,new_x,new_y,new_x2,new_y2)

        -- center location of segment
        loc = torch.Tensor{0,0}

        -- apply the glimpse
        local output = sg:forward{img_seg,loc}
        local glimpse_imgs = torch.chunk(output,num_depths)

        local num_glimpses = #glimpse_imgs

        local gl_imgs = torch.Tensor(num_glimpses,3,config.glimpse_size,config.glimpse_size)
        for t=1,num_glimpses,1 do
          gl_imgs[{ {t},{},{},{} }] = normalizeImg(glimpse_imgs[t],probe_mean,probe_std)
        end

        --glimpse_imgs = torch.Tensor(glimpse_imgs)

        -- TODO: normalize the glimpse images with pcn_models mean and standard deviation


        -- TODO: Apply the network model to the set of glimpses
        -- TODO: obtain confidence scores
        -- TODO:Load the current model
        for t=1,num_glimpses,1 do
          local scores_per_patch = pcn_model:forward(gl_imgs[t]:cuda()):squeeze()
          -- Get the top 5 class indexes and probabilities

          local probs, indexes = scores_per_patch:topk(1, true, true)
          --print('Classes for segment',i,j,t)
          --print(probs[1], set_of_classes[indexes[1]])
          class_count[indexes[1]] = class_count[indexes[1]] + 1
        end
        -- TODO: voting among glimpses :- best of 3; from histogram of classes

        -- TODO: store segment region location, size, probability value and score as a JSON file.

        -- TODO: Accumulate scores
        --scores[segment_count] = scores_per_patch
      end
    end

  end
end

--val, final_class_ind = torch.max(class_count,1)
--print(set_of_classes)
--print("Class of portion : ", i,j)
--print(set_of_classes[final_class_ind[1]])
--print(class_count)
--print('')

-- print out/output percentage of
norm_class_count = class_count*100 / torch.sum(class_count)
local results = {}
for i,v in ipairs(set_of_classes) do
  local norm_count = norm_class_count[i]
  local result_per_class = {class= v, hitPercentage= norm_count}
  --results[i] = {"class"= v, "hit percentage" = norm_class_count[i]}
  results[i] = result_per_class
  --print(results[i])
end

-- store the results as JSON file
serializedString = json.encode(results )
print(serializedString)
local result_file = io.open(config.resultFile, "w")
result_file:write(serializedString)
io.close(result_file)
