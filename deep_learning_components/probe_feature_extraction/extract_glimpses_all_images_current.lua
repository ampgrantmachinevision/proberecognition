-- New Updated File for obtaining image features
require 'torch'
require 'nn'
require 'image'
require 'cutorch'
require 'cunn'
require 'cudnn'
require 'dpnn'

collectgarbage();collectgarbage()

local cmd = torch.CmdLine()

-- TODO: set options to read train/test images and create train/test features
-- TODO: Train features accumulated across all the training images (save them)
cmd:option('-trainfile','data/train.txt','path to where the train files are stored')
cmd:option('-testfile','data/test.txt','path to where the test files are stored')
cmd:option('-allfile','data/files.txt','path to where the test files are stored')
cmd:option('-noSplit',true, 'flag to load all files with no train test split')
cmd:option('-savePath','data/images','path where the images are stored')
cmd:option('-setOfClasses','data/probe_component_classes.txt','File containing the names of the classes')

-- TODO: Network fine-tuned to recognize probe regions
-- TODO: Bag of words model from training features
-- TODO: Represent each training/test image as a bag of words
-- TODO: Histogram of codewords
-- TODO: K-Nearest neighbor classifier to get classification accuracy.
cmd:text()
cmd:text('Operation of Glimpse module on image:')
cmd:text()
cmd:text('Options:')
cmd:option('-img','data/sample.png','path to where image resides')
cmd:option('-ndepths',3,'the depth of the glimpse') -- controls how many layers are there
cmd:option('-nscales',2,'the number of scales') -- controls how much resolution is reduced at each depth
cmd:option('-glimpse_size',128,'size of the glimpse')
cmd:option('-crop_size',512,'Size of cropped region')

-- options for deep mask
cmd:option('-model','pretrained/deepmask','path to model to load')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 10,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')

-- options for output file names
cmd:option('-glimpses','data/glimpses-','path to store the features without t7 extension')
cmd:option('-probe_component_labels','data/component_labels-','path to store labels of probe components without t7 extension')

local config = cmd:parse(arg)

-- Load the probe component class file
classes_file = torch.DiskFile(config.setOfClasses,"r")
classes_file:quiet()
num = 1
set_of_classes = {}
repeat
  classes_str = classes_file:readString("*l")
  set_of_classes[num] = classes_str
  num = num+1
until classes_str == ''
set_of_classes[num-1] = nil

-- Create the output image directory
save_path_dirs = {}
for i,v in pairs(set_of_classes) do
  dir_name = string.format('%s/%s',config.savePath,set_of_classes[i])
  success = paths.mkdir(dir_name)
  if success then
    print('directory created')
  else
    print('directory already present')
  end
  save_path_dirs[i] = dir_name
end
print(save_path_dirs)

-- READ THE TRAIN TEXT FILES
train_file = io.open(config.trainfile)
print(train_file)
-- read all the train file names
local count = 1
local train_img_files = {}
local train_img_class_labels = {}
--io.input(train_file)
--print(config.trainfile)
while true do
  local line = train_file:read()
  if line == nil then
    break
  end
  --print(line)
  k = string.find(line,'\t')
  img_file_name = string.sub(line,1,k-1)
  img_class = string.sub(line,k+1,-1)

  -- TODO: Parse the string and get the image file name and store as a table
  --print(img_file_name)
  --print(img_class)

  train_img_files[count] = img_file_name
  train_img_class_labels[count] = img_class

  -- TODO: Store the path of image as a string
  count = count + 1
end
train_file:close(train_file)

--print(#train_img_files)
--print(#train_img_class_labels)

-- READ THE TEST TEXT FILES
test_file = io.open(config.testfile)
print(test_file)
-- read all the train file names
count = 1
local test_img_files = {}
local test_img_class_labels = {}
--io.input(train_file)
--print(config.testfile)
while true do
  local line = test_file:read()
  if line == nil then
    break
  end
  --print(line)
  k = string.find(line,'\t')
  img_file_name = string.sub(line,1,k-1)
  img_class = string.sub(line,k+1,-1)

  -- TODO: Parse the string and get the image file name and store as a table
  --print(img_file_name)
  --print(img_class)

  test_img_files[count] = img_file_name
  test_img_class_labels[count] = img_class

  -- TODO: Store the path of image as a string
  count = count + 1
end
test_file:close(test_file)

--print(#test_img_files)
--print(#test_img_class_labels)

-- READ THE ALL TEXT FILES
all_file = io.open(config.allfile)
--print(all_file)
-- read all the train file names
count = 1
local all_img_files = {}
local all_img_class_labels = {}
--io.input(train_file)
--print(config.allfile)
while true do
  local line = all_file:read()
  if line == nil then
    break
  end
  --print(line)
  k = string.find(line,'\t')
  img_file_name = string.sub(line,1,k-1)
  img_class = string.sub(line,k+1,-1)

  -- TODO: Parse the string and get the image file name and store as a table
  --print(img_file_name)
  --print(img_class)

  all_img_files[count] = img_file_name
  all_img_class_labels[count] = img_class

  -- TODO: Store the path of image as a string
  count = count + 1
end
all_file:close(all_file)

print(#all_img_files)
print(#all_img_class_labels)

list_of_image_files = {}
list_of_image_labels = {}
list_of_image_files[1] = train_img_files
list_of_image_files[2] = test_img_files
list_of_image_files[3] = all_img_files
list_of_image_labels[1] = train_img_class_labels
list_of_image_labels[2] = test_img_class_labels
list_of_image_labels[3] = all_img_class_labels

-------------------DEEP MASK INITIALIZATION------------------------------------------------
-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

--------------------------------------------------------------------------------
-- load model
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.model)
local m = torch.load(config.model..'/model.t7')
local model = m.model
model:inference(config.np)
model:cuda()

--------------------------------------------------------------------------------
-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}

--------------------DEEP MASK END--------------------------------------
--------------------- PRETRAINED MODEL FOR FEATURE EXTRACTION ----------------
--local feature_extractor
--if cutorch.getDeviceCount() > 0 then
--  feature_extractor = torch.load('pretrained/resnet-18.t7'):cuda()
--else
--  feature_extractor = torch.load('pretrained/resnet-18.t7')
--end

---- removing the last layer
--assert(torch.type(feature_extractor:get(#feature_extractor.modules))=='nn.Linear')
--feature_extractor:remove(#feature_extractor.modules)

---- put in evaluate mode
--feature_extractor:evaluate()

-------------------- FEATURE EXTRACTION  -----------------------------------
-- load the image

local num_depths = config.ndepths;
local num_scales = config.nscales;
local glimpse_size = config.glimpse_size;
local crop_size = config.crop_size;

-- resize image to 768 x 1792 or 512 x 1280

-- TODO :Iterate through the set of train images
local img_set_list = {"train","test","all"}

-- doing for all the files
for s = 3,3,1 do --1,#list_of_image_files,1 do
  local img_file_list = list_of_image_files[s]
  local probe_class_list = list_of_image_labels[s]
  s_mesg = string.format("Number of %s files : %d",img_set_list[s],#img_file_list)
  print(s_mesg)
  --local features = {}
  --local component_labels = {}
  --local glimpse_imgs = {}

  for img_num = 1,#img_file_list,1 do
    local  img_file_name = img_file_list[img_num]
    local probe_class = probe_class_list[img_num]
    print(img_file_name)
    --paths.dofile(img_file_name)

    local img = image.load(img_file_name)
    img = image.scale(img,768,1792)
--print(#img)

-- obtain glimpses of each of the image
    sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)

-- Extract 512 x 512 sub-segments of image and run deep mask to get possible object-centric regions.
    total_segments = 0
    num_glimpses_per_img = 0;
    --local glimpses_per_img = torch.ByteTensor(500,img:size(1),glimpse_size,glimpse_size) --  to save space; store images as unsigned char values
    --local labels_per_img = torch.IntTensor(500,2):zero() -- first column provides labels for probe component, and the second for the configuration

    local num_segment_rows = img:size(2)/ (crop_size/2) -- 7
    local num_segment_cols = img:size(3)/ (crop_size/2) -- 3

    local total_segments = 0
    local num_glimpses_per_img = 0

-- TODO: Create appropriate labels for features of that segment : probe stylus - 1, probe body - 2, probe head - 3 ; Depends on the row index - i = 1,2 : class - 1, i = 3,4 : class - 2; i = 5,6 - class - 3

    for i=1,num_segment_rows-1,1 do
      for j=1,num_segment_cols-1,1 do
        h = crop_size
        w = crop_size
        x1 = (j-1)*crop_size/2  -- zero based indexing x
        y1 = (i-1)*crop_size/2 -- zero based indexing y
        x2 = x1 + w
        y2 = y1 + h

        img_crop_level2 = image.crop(img,x1,y1,x2,y2)

        -- Apply deep mask
        infer:forward(img_crop_level2)

        local masks,scores = infer:getTopProps(.2,h,w)
        --print(scores)
        --print(#masks)
        -- save result
        local res = img_crop_level2:clone()

        --Iteratively draw the mask on the image based on the score
        local val_count = 0
        for k=1,config.np,1 do
          score_per_mask = scores[k][1]
          if score_per_mask > 0.2 then
            val_count = val_count + 1
          end
        end

        if val_count > 0 then
          masks_new = masks:sub(1,val_count)
          maskApi.drawMasks(res, masks_new) -- draws only relevant features

          -- encode the mask
          masks_rs = maskApi.encode(masks_new)
          -- get the bounding boxes
          bbs = maskApi.toBbox(masks_rs)

          -- Iterate through each bounding box
          for k = 1,bbs:size(1),1 do
            x = bbs[k][1]
            y = bbs[k][2]
            w = bbs[k][3]
            h = bbs[k][4]

            cent_x = x + w/2
            cent_y = y + h/2

            new_x = math.max(x1 + cent_x -255,1)
            new_y = math.max(y1 + cent_y - 255,1)
            new_x2 = math.min(x1 + cent_x + 256,img:size(3))
            new_y2 = math.min(y1 + cent_y + 256,img:size(2))

--            -- get the segment
--            local img_new2 = torch.Tensor(img:size(1),img:size(2),img:size(3)):zero()
--            img_new2 = torch.maskedCopy(masks_new[k],img)
            --local img_seg = image.crop(img_new2,new_x,new_y,new_x2,new_y2)

            -- segment for the glimpse
            local img_seg = image.crop(img,new_x,new_y,new_x2,new_y2)

            -- center location of segment
            loc = torch.Tensor{0,0}

            -- apply the glimpse
            local output = sg:forward{img_seg,loc}
            --local output = sg:forward{img_crop_level2,loc}
            local outputs = torch.chunk(output,num_depths)
            print(#outputs)

            for m=1,#outputs,1 do

              --glimpse_img = image.toDisplayTensor(outputs[m])
              local glimpse_img = outputs[m]:clone()
              -- print(torch.type(glimpse_img))

              -- convert to FloatTensor from LongStorage

--              -- TODO: Preprocess each output, and normalize each channel
--              for c = 1,3 do
--                glimpse_img[c]:add(-meanstd.mean[c])
--                glimpse_img[c]:div(meanstd.std[c])
--              end
              --glimpse_img2 = (glimpse_img * 255):byte()
              num_glimpses_per_img = num_glimpses_per_img + 1


              --glimpses_per_img[{num_glimpses_per_img,{},{},{} }] = glimpse_img
              --glimpses_per_img[num_glimpses_per_img] = glimpse_img2
              --print(#glimpse_img)


              -- set the label
              -- TODO: This should not be hard-coded!!!
              glimpse_label = 0
              local function value_present(tb,vl)
                local found = false
                for i,v in ipairs(tb) do
                  if vl == v then
                    found = true
                  end
                end

                return found
              end

              -- Assigning true class labels to the probe
              if i >= 1 and i < num_segment_rows/3 then
                -- select stylus 1,2 or 3
                if value_present({1,4,7,10},tonumber(probe_class)) then
                  glimpse_label = 1
                elseif value_present({2,5,8,11},tonumber(probe_class)) then
                  glimpse_label = 2
                elseif value_present({3,6,9,12},tonumber(probe_class)) then
                  glimpse_label = 3
                end
              elseif i >= num_segment_rows/3 and i < 2*num_segment_rows/3 then
                -- select body 1 or 2
                if value_present({1,2,3,7,8,9},tonumber(probe_class)) then
                  glimpse_label = 4
                elseif value_present({4,5,6,10,11,12},tonumber(probe_class)) then
                  glimpse_label = 5
                end
              elseif i >= 2*num_segment_rows/3 and i < num_segment_rows then
                -- select head 1 or 2
                if value_present({1,2,3,4,5,6},tonumber(probe_class)) then
                  glimpse_label = 6
                elseif value_present({7,8,9,10,11,12},tonumber(probe_class)) then
                  glimpse_label = 7
                end
              end
              print("Glimpse label")
              print(glimpse_label)
              print("Probe class")
              print(probe_class)

              if(save_path_dirs[glimpse_label] == nil) then
                print('something wrong')
              end

              glimpse_img_filename = string.format('%s/imageNum-%d-image-seg-%d.png',save_path_dirs[glimpse_label],img_num,num_glimpses_per_img)
              mesg = string.format('Glimpse img: %s saved',glimpse_img_filename)

              image.save(glimpse_img_filename,glimpse_img)
              print(mesg)

              -- TODO: Save the image into the appropriate folder
            end

            local img_glimpse_crop = image.toDisplayTensor(outputs)

            --save_file_name =string.format("%s_%d_%d_glimpse_%d.png",img_segment_base_name,i,j,k)
            --image.save(save_file_name,img_glimpse_crop)
            --print(save_file_name)

            total_segments = total_segments+1


          end

        end

        --save_file_name =string.format("%s_%d_%d.png",img_segment_base_name,i,j)
        --image.save(save_file_name,res)
        --print(save_file_name)

      end
    end

    --if num_glimpses_per_img > 200 then
    --  num_glimpses_per_img = 200
    --end
    --glimpses_per_img = glimpses_per_img[{ {1,num_glimpses_per_img},{},{},{} }]
    --labels_per_img = labels_per_img[{ {1,num_glimpses_per_img}, 1 }]

    local mesg_str = string.format("Number of (segments,glimpses) for image %s : (%d,%d)",config.img,total_segments,num_glimpses_per_img)
    print(mesg_str)
--print(#glimpses_per_img)

-- truncating to actual size

--print(#glimpses_per_img)

--------------------- PRETRAINED MODEL FOR FEATURE EXTRACTION ----------------
----local feature_extractor
--    print('--- EXTRACTING FEATURES---')
--    local features_per_img

--    local glimpses_per_img_float = glimpses_per_img:float():cuda()

--    local output = feature_extractor:forward(glimpses_per_img_float):squeeze(1)

---- this is necesary because the model outputs different dimension based on size of input
--    if output:nDimension() == 1 then output = torch.reshape(output, 1, output:size(1)) end
--    --print(output[1])

---- STORE THE FEATURES PER IMAGE
--    features_per_img = torch.FloatTensor(num_glimpses_per_img,output:size(2))
--    features_per_img = features_per_img:copy(output)
--    --print(features_per_img[1])

--    output = nil
--    glimpses_per_img_float = nil

------------------- PRETRAINED MODEL FOR FEATURE EXTRACTION END ---------------

---- save the glimpses of the image
    --torch.save('data/glimpses_per_img.t7',glimpses_per_img)

--    features[img_num]= features_per_img
    --glimpse_imgs[img_num] = glimpses_per_img:clone()
    --component_labels[img_num] = labels_per_img:clone()

    -- TODO: save each glimpse into a folder which is divided into three classes

    glimpses_per_img = nil
    labels_per_img = nil

    s_mesg = string.format('Glimpses computed for image : %s, Image Number: %d \n',img_file_name,img_num)
    print(s_mesg)

    collectgarbage();collectgarbage()
  end
  --glimpse_save_name = string.format("%s%s.t7",config.glimpses,img_set_list[s])
  --component_labels_save_name = string.format("%s%s.t7",config.probe_component_labels,img_set_list[s])

  --torch.save(component_labels_save_name,component_labels)
  --torch.save(glimpse_save_name,glimpse_imgs)

  --s_mesg = string.format('%s saved', glimpse_save_name)
  --print(s_mesg)
end
collectgarbage()

