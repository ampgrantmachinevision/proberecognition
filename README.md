# Probe Component Recognition
=============================

The project contains two main folders, Qt-framework and deep-learning-components. The first one is for the C++ deployable prototype built using the Qt framework, and the second is for deep learning code built using Torch framework. The C++ prototype reads in an image from a camera or a set of images from a directory, calls the Torch rountine to perform Probe Component Recognition. The expected output from running the prototype is an image annotated with regions/segments with labels as to what those regions represent. Currently, it would read "head-1","head-2","stylus-1", "stylus-2", "stylus-3","body-1", and "body-2", since its trained only on those type of components. 

## Installation Instructions
The minimum requirement for this project to run successfully is to have a Linux computer with NVidia graphics card with CUDA compatibility > 7.0 with a required set of drivers, libraries, and softwares. These are the following:
-	Drivers for NVidia/CUDA: Cuda-7.5 or greater drivers with instructions and code available at [Cuda download](https://developer.nvidia.com/cuda-downloads). Preferably install the .deb version. In addition, add the following line to the ~/.bashrc file in your home folder. "export PATH=/usr/local/cuda/bin:$PATH" . Also, add the following line to /etc/ld.so.conf file : "/usr/local/cuda/lib64". Then run the following commands at the terminal. 

~~~
sudo ldconfig
source ~/.bashrc
~~~ 

-	Next, install the Cudnn libraries which can either be downloaded from [Cudnn download](https://developer.nvidia.com/rdp/cudnn-download) for the latest version, or use the one provided in the source directory. Run the following commands on terminal.
~~~
tar -xvf cudnn-8.0-linux-x64-vx.x
sudo cp -P cuda/include/cudnn.h /usr/local/cuda/include
sudo cp -P cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo ldconfig
~~~

-	Install gcc-5 and g++-5 is not available in the system. For 16.04, its default but in 16.10 it needs to be installed as gcc-6 version is the default. Follow the code below to set gcc-5 as the default, and gcc-6 as the option for selection.
~~~
sudo apt-get install gcc-5 g++-5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 100 --slave /usr/bin/g++ g++ /usr/bin/g++-5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 50 --slave /usr/bin/g++ g++ /usr/bin/g++-6
~~~

-	To select appropriate gcc version, use the following command and on-screen instructions.
~~~
sudo update-alternatives --config gcc
~~~
	
-	Download and Install torch deep learning framework into your home folder. Follow instructions as given in [Torch Installation](http://torch.ch/docs/getting-started.html#_). These commands are the following:
~~~
git clone https://github.com/torch/distro.git ~/torch --recursive
cd ~/torch; bash install-deps;
./install.sh
source ~/.bashrc
~~~

-	Once completed, run the following commands to install the additional packages in torch for the program to run within the ~/torch directory.
~~~
luarocks install image
luarocks install optim
luarocks install dp
luarocks install nnx
luarocks install inn
luarocks install tds
luarocks install json
luarocks install cutorch
luarocks install cunn
luarocks install cudnn
~~~

-	(Optional) For installation of COCO API torch library, follow the instructions in [COCO API](https://github.com/pdollar/coco). Specifically, follow the code below inside the torch parent directory.
~~~
mkdir -p API
git clone https://github.com/pdollar/coco
cd coco
luarocks make LuaAPI/rocks/coco-scm-1.rockspec 
~~~
-	Install OpenCV-2.4 on Ubuntu 16.04 or 16.10. 
~~~
sudo apt-get install libopencv*
~~~ 
-	(Optional) Install the Qt libraries : Qt 5.8 or greater from Qt website if releveraging Qt-Creator and Qt-Cross compilation toolchain. This will require an account with Qt for either open-source or commercial. 

## Training (Probe Feature Extraction and Deep Neural Network Training)
------------------------------------------------
The various scripts for training the system are provided in the folder [deep_learning_components/probe_feature_extraction](). The training of the algorithm involves the following steps :
 
-	Reading of images stored according to its configuration, and generate files containing the location of the train, test and validation categories. Example of input directory structure : [<images>/<probe_configuration_class>/<filename_timestamp>.png](). The folder must contain images which are arranged by class name and a text file containing the names of the various classes. Eg: TP20-Ext1-Stylus1, TP20-Ext2-Stylus2, etc.. A reference C++ code of doing this task is provided in the function `ProcessEngine::trainTestSplit` present in file [Qt-Framework/ProbeRecognition_MVP1/processengine.cpp](). The resulting files are [train.txt](),[test.txt](), and [files.txt]() within the folder containing the images. Copy them to the folder [deep_learning_components/probe_feature_extraction/data]().
-	Extract the probe features from the images using the created [train.txt](),[test.txt](), and [files.txt](). Note that the output results in creation of a folder [deep_learning_components/probe_feature_extraction/data/images]() which contains a set of glimpse images arranged according to specific probe component classes such as Stylus-1, Stylus-2,...head1, head2. Copy the text file [probe_component_classes_specific.txt]() present in the data folder to the [data/images]() folder. Run the train and test split code to generate [train_ProbeSegments.txt](), and [test_ProbeSegments.txt](). Copy these text files back to the data folder. An example of extracting and saving 64 x 64 glimpses at 3 different scales is shown below. 
~~~
cd ProbeRecognition/deep_learning_components/probe_feature_extraction
th extract_glimpses_all_images_current.lua -glimpse_size 64 -nscales 3
~~~
-	Using the [train_ProbeSegments.txt](), and [test_ProbeSegments.txt](), create appropriate data source model (DataSource) component which creates 'train', 'test', and 'valid' folders. This is done by runing the code shown below.
~~~
th DataOrganization.lua -trainFile 'data/train_ProbeSegments.txt' -testFile 'data/test_ProbeSegments.txt' -dataPath 'data/ProbeSegments_TrainTestSplit' -setOfClasses 'data/probe_component_classes_specific.txt'
~~~
-	Train the probe component network using the Torch 'dp' package and save the model for inference. Sample code for doing this task is shown in Figure.
~~~
th Final_TrainProbeComponentSpecific.lua
~~~
-	To make sure the right paramters (correct file paths,etc.) are given for the code, please run the code with the flag `--help`. Example is shown below.
~~~
th extract_glimpses_all_images_current.lua --help
th DataOrganization.lua --help
th Final_TrainProbeComponentSpecific.lua --help
~~~

## Testing (Probe Component Detection)
The various scripts for training the system are provided in the folder [deep_learning_components/probe_component_detection](). The trained model file [probenet.t7]() and the meta data file [meanstd.th7]() are copied over the [data]() folder within this testing directory. The data folder within this directory also contains sample images. The inference program [InferPCN.lua]() does the probe feature extraction and probe component classification and generates a normalized scores across the probe component classes as a JSON file. Sample code to run the inference is shown below.
~~~
cd ProbeRecognition/deep_learning_components/probe_component_detection
th InferPCN.lua -pcn_model 'trained_models/probenet.t7' -meanstd 'trained_models/meanstd.th7' -glimpse_size 64 -resultFile 'data/results.json' 
~~~

