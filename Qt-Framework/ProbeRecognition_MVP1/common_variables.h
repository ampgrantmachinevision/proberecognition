#ifndef COMMON_VARIABLES_H
#define COMMON_VARIABLES_H

#define INPUT_WIN "Image"
#define RESIZED_IMG_FOLDER "Set1"

#define IMAGENETSIZE 256
#define SCALE_FACTOR 3

#define WORKING_DIR "/home/nairb1/workspace/Git/AMP_Grant/deepmask" // This needs to be read from a config file.
#define SEGMENTATION_MODEL_DIR "/pretrained/deepmask"
#define DATA_DIR "/data"
#define NUM_PROPOSALS 5

#endif // COMMON_VARIABLES_H
