-- Probe Component Classifier
require 'torch'
require 'nn'
require 'nnx'
require 'cunn'
require 'cudnn'
require 'dpnn'
require 'dp'
require 'cutorch'

cudnn.benchmark = true
cudnn.fastest = true
cudnn.verbose = true

local cmd = torch.CmdLine()
cmd:text("Training of Probe Component Classifier")
cmd:text("")

cmd:option('-dataSourceFile','data/ProbeComponentDataSource.t7','The data source model to be loaded')

cmd:option('--learningRate', 0.01, 'learning rate at t=0')
cmd:option('--lrDecay', 'linear', 'type of learning rate decay : adaptive | linear | schedule | none')
cmd:option('--minLR', 0.00001, 'minimum learning rate')
cmd:option('--saturateEpoch', 300, 'epoch at which linear decayed LR will reach minLR')
cmd:option('--schedule', '{}', 'learning rate schedule')
cmd:option('--maxWait', 4, 'maximum number of epochs to wait for a new minima to be found. After that, the learning rate is decayed by decayFactor.')
cmd:option('--decayFactor', 0.001, 'factor by which learning rate is decayed for adaptive decay.')
cmd:option('--maxOutNorm', 1, 'max norm each layers output neuron weights')
cmd:option('--momentum', 0.9, 'momentum')
cmd:option('--hiddenSize', '{1000}', 'number of hidden units per layer')
cmd:option('--batchSize', 100, 'number of examples per batch')
cmd:option('--cuda', true, 'use CUDA')
cmd:option('--useDevice', 1, 'sets the device (GPU) to use')
cmd:option('--maxEpoch', 500, 'maximum number of epochs to run')
cmd:option('--maxTries',100, 'maximum number of epochs to try to find a better local minima for early-stopping')
cmd:option('--dropout', false, 'apply dropout on hidden neurons')
cmd:option('--batchNorm', false, 'use batch normalization. dropout is mostly redundant with this')
cmd:option('--standardize', true, 'apply Standardize preprocessing')
cmd:option('--zca', false, 'apply Zero-Component Analysis whitening')
cmd:option('--progress', false, 'display progress bar')
cmd:option('--silent', false, 'dont print anything to stdout')
cmd:text()

local config = cmd:parse(arg or {})
config.schedule = dp.returnString(config.schedule)
config.hiddenSize = dp.returnString(config.hiddenSize)
print('Configuration of Training')
table.print(config)

-- load the datasource
ds = torch.load(config.dataSourceFile)
print(ds)

cutorch.setDevice(config.useDevice)

config.numOutputs = #ds:classes()
config.useCompleteModel = true

------------------------------ NETWORK DEFINITION ---------------------------
-- define the Fully connected network
-- TODO: Define the network here, but later define in another file which can be read.
-- code reference : https://github.com/facebook/fb.resnet.torch/blob/master/models/resnet.lua
local function basicResidualBlock(nIn,nOut,stride)
  -- convolution branch
  local resBlock_br1 = nn.Sequential()

  -- adding the layer
  resBlock_br1:add(cudnn.SpatialConvolution(nIn,nOut,3,3,stride,stride,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))
  resBlock_br1:add(cudnn.ReLU())
  resBlock_br1:add(cudnn.SpatialConvolution(nOut,nOut,3,3,1,1,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))

  -- indentity branch

  -- Create a layer to house the two branches
  local resBlock = nn.ConcatTable()
  resBlock:add(resBlock_br1)
  if nIn ~= nOut then
    resBlock:add(cudnn.SpatialConvolution(nIn,nOut,1,1,stride,stride))
    --resBlock_br2:add(cudnn.SpatialBatchNormalization(nOut))
  else
    resBlock:add(nn.Identity())
  end

  local finalResBlock = nn.Sequential()
  finalResBlock:add(resBlock)
  finalResBlock:add(nn.CAddTable(true))
  finalResBlock:add(cudnn.ReLU(true))

  return finalResBlock
end

local function define_net(config)
-- define local functions which define the residual layer (basic block
  local model
  if(config.useCompleteModel == true) then
    -- Load the pre-trained model
    model = torch.load('pretrained/resnet-18.t7')

    -- remove the fully connected layer
    model:remove(#model.modules)
    model:remove(#model.modules)
    model:remove(#model.modules)

    -- remove the last two residual layers which models block by block relations (conv#4 and conv#5)
    model:remove(#model.modules)
    model:remove(#model.modules)

  else
    model = nn.Sequential()
    -- output of feature map is now b x 128 x 28 x 28
  end

  -- define 4th conv residual layers
  model:add(basicResidualBlock(128,256,2))
  model:add(basicResidualBlock(256,256,1))

  -- define the 5th conv residual layer
  model:add(basicResidualBlock(256,512,2))
  model:add(basicResidualBlock(512,512,1))

  -- add the spatial average pooling filter
  model:add(nn.SpatialAveragePooling(7,7,1,1))
  model:add(nn.View(512):setNumInputDims(3))

  -- add the fully connected layer
  local num_inputs = 512
  local num_hiddenUnits
  local num_outputs = config.numOutputs
  for i,v in ipairs(config.hiddenSize) do
    num_hiddenUnits = v
    model:add(nn.Linear(num_inputs,num_hiddenUnits))
    model:add(cudnn.ReLU())
    num_inputs = num_hiddenUnits
  end
  model:add(nn.Linear(num_hiddenUnits,num_outputs))
  model:add(nn.LogSoftMax())

  return model
end

net_mainbranch = define_net(config)
net_complete = nn.Sequential()
net_complete:add(nn.Convert(ds:ioShapes(),'bchw'))

net_complete:add(net_mainbranch)
--net_mainbranch = net_mainbranch:cuda()
print(net_complete)


------------------------- END NETWORK DEFINITION ----------------------------

------------------ TRAINING AND EVALUATION ---------------------------------
if config.lrDecay == 'adaptive' then
  ad =dp.AdaptiveDecay{max_wait = config.maxWait, decay_factor = config.decayFactor}
elseif config.lrDecay == 'linear' then
  config.decayFactor = (config.minLR - config.learningRate)/config.saturateEpoch
end

-- Define the Optimizer for ,Evaluator for validation and testing
train = dp.Optimizer{
  acc_update = config.accUpdate,
  -- define loss
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),

  -- function to be called every epoch
  epoch_callback = function(model,report)
    -- update the learning rate after one iteration of all batches
    if report.epoch > 0 then
      if config.lrDecay == 'adaptive' then
        config.learningRate = config.learningRate *ad.decay
        ad.decay = 1
      elseif config.lrDecay == 'schedule' and config.schedule[report.epoch] then
        config.learningRate = config.schedule[report.epoch]
      elseif config.lrDecay == 'linear' then
        config.learningRate = config.learningRate + config.decayFactor
      end
      config.learningRate = math.max(config.minLR, config.learningRate)
      if not config.silent then
        print("learningRate",config.learningRate)
      end
    end
  end,

  -- call back function for every batch
  -- TODO: Maybe just use the optim libraries to optimize
  -- put here, to just re-train the last few layers
  -- the weights the other layers remain constant.
  callback = function(model,report)
    if config.accUpdate then
      model:accUpdateGradParameters(model.dpnn_input,model.output,config.learningRate)
    else
      model:updateGradParameters(config.momentum) -- affects gradients of the weights
      model:updateParameters(config.learningRate)
    end
    model:maxParamNorm(config.maxOutNorm) -- normalize the outputs of each neuron at each layer
    model:zeroGradParameters() -- zeros the gradients of the weights
  end,
  feedback = dp.Confusion(), -- provides accuracy
  sampler = dp.ShuffleSampler{batch_size = config.batchSize}, -- shuffle the indices
  progress = config.progress
}

-- Now the evaluator for the validation and test set
valid = dp.Evaluator{
  -- define loss
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),
  feedback = dp.Confusion(),
  sampler = dp.Sampler{batch_size = config.batchSize}
}

test = dp.Evaluator{
  loss = nn.ModuleCriterion(nn.ClassNLLCriterion(),nil,nn.Convert()),
  feedback = dp.Confusion(),
  sampler = dp.Sampler{batch_size = config.batchSize}
}

-- setting up the experiment
xp = dp.Experiment{
  model = net_complete,
  optimizer = train,
  validator = valid,
  tester = test,
  observer = {
    dp.FileLogger(),
    dp.EarlyStopper{
      error_report = {'validator','feedback','confusion','accuracy'},
      maximize = true,
      max_epochs = config.maxTries
    }
  },
  random_seed = os.time(),
  max_epoch = config.maxEpoch
}

print(xp)
xp:cuda()
xp:run(ds)