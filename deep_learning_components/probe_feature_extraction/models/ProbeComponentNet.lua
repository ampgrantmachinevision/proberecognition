-- Probe Net
------------------------------ NETWORK DEFINITION ---------------------------
-- define the Fully connected network
-- TODO: Define the network here, but later define in another file which can be read.
-- code reference : https://github.com/facebook/fb.resnet.torch/blob/master/models/resnet.lua
local function basicResidualBlock(nIn,nOut,stride)
  -- convolution branch
  local resBlock_br1 = nn.Sequential()

  -- adding the layer
  resBlock_br1:add(nn.SpatialConvolution(nIn,nOut,3,3,stride,stride,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))
  resBlock_br1:add(nn.ReLU())
  resBlock_br1:add(nn.SpatialConvolution(nOut,nOut,3,3,1,1,1,1))
  resBlock_br1:add(nn.SpatialBatchNormalization(nOut))

  -- indentity branch

  -- Create a layer to house the two branches
  local resBlock = nn.ConcatTable()
  resBlock:add(resBlock_br1)
  if nIn ~= nOut then
    resBlock:add(nn.SpatialConvolution(nIn,nOut,1,1,stride,stride))
    --resBlock_br2:add(cudnn.SpatialBatchNormalization(nOut))
  else
    resBlock:add(nn.Identity())
  end

  local finalResBlock = nn.Sequential()
  finalResBlock:add(resBlock)
  finalResBlock:add(nn.CAddTable(true))
  finalResBlock:add(nn.ReLU(true))

  return finalResBlock
end

function create_model(config)
  -- define local functions which define the residual layer (basic block
  local net = nn.Sequential()
  numChannels = config.load_size[1]
  numRows = config.load_size[2]
  numCols = config.load_size[3]
  numBatches = 5

  convFilterChannelSize = math.floor(math.max(numRows,numCols) / 4)

  -- create sample image
  local sampleData = torch.Tensor(numBatches,numChannels,numRows,numCols):zero()

  -- input is 3 @ 64 x 64 => 16 @ 32 x 32
  net:add(nn.SpatialConvolution(numChannels,convFilterChannelSize,3,3,1,1,1,1))
  net:add(nn.SpatialBatchNormalization(convFilterChannelSize))
  --net:add(nn.SpatialMaxPooling(2,2,2,2)) -- output is 16 @ 32 x 32
  net:add(nn.ReLU()) -- output is 16 @ 32 x 32

  -- adding dropout before input to next layer
  --net:add(nn.Dropout(config.dropOutProb))

  -- Two residual blocks
  -- input is 16 @ 32 x 32 -> 32 @ 16 x 16
  -- TODO: create an input which takes in number of residual layers
  net:add(basicResidualBlock(convFilterChannelSize,convFilterChannelSize*2,2))
  net:add(basicResidualBlock(convFilterChannelSize*2,convFilterChannelSize*4,2)) -- output is 64 @ 8 x 8
  net:add(basicResidualBlock(convFilterChannelSize*4,convFilterChannelSize*8,2)) -- output is 128 @ 4 x 4
  net:add(nn.SpatialAveragePooling(2,2,2,2)) -- 128 @ 2 x2

  net:add(nn.View(-1):setNumInputDims(3))

  numInputsFCC = #net:forward(sampleData)

  -- fully connected layer
  net:add(nn.Linear(numInputsFCC[2],1000))
  net:add(nn.ReLU())
  --net:add(nn.Dropout(config.dropOutProb))
  net:add(nn.Linear(1000,config.outputSize))
  net:add(nn.LogSoftMax())

  return net

end