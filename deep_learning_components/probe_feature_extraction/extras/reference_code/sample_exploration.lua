require 'torch'
require 'cutorch'
require 'cudnn'
require 'dpnn'
require 'nn'
require 'image'

-- This is a sample file which will read in images, and build a recurrent model for visual attention
-- Mainly to aquaint with the Glimpse module, and LSTM module required for probe recognition
-- We won't be using the Reinforce algorithm and won't be using recurrent model as such for probe.
-- Each image is a 256 x 256 patch on a high resolution imagery.

-- IDEA 1: The glimpse module can focus on region of interest, and obtain 9 x 64 x 64 set of images.
-- Network will be -> glimpse module -> convolutional layers-> LSTM layer (single) -> probe label
-- Backpropagation with weight update for every large image (for every T=84 steps of image)
-- Images are stored in the folder : all images, label file -> image name and class number.

-- Sample exploration of Glimpse module on the probe image and lena image.

-- First on the Lena Image
img = image.lena()

loc = torch.Tensor{0,0} -- setting the location
num_depths = 3;
num_scales = 2;
glimpse_size = 64;

sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)

output = sg:forward{img,loc}
print(output:size())

-- unroll the glimpse onto different depths
outputs = torch.chunk(output,3)
display = image.toDisplayTensor(outputs)

image.display(display)
image.save("data/glimpse-output.png",display)

--- NEW SECTION OF THE CODE
-- Now, get a small section  image of probe, run deep mask on the sub-image, get the location of the topmost proposal, and get Glimpse of probe part.

-- reading the image

-- divide the image into segments


num_depths = 3
num_scales = 2
glimpse_size = 64

sg = nn.SpatialGlimpse(glimpse_size,num_depths,num_scales)



-- display the probe
-- unroll the glimpse onto different depths
outputs = torch.chunk(output,3)
display = image.toDisplayTensor(outputs)

image.display(display)
image.save("glimpse-output-probe.png",display)

