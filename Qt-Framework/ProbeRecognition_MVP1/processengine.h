#ifndef PROCESSENGINE_H
#define PROCESSENGINE_H

#include <QObject>
#include "proberecognizer.h"
#include <algorithm>
#include <ctime>
#include <cstdlib>

class ProcessEngine : public QObject
{
    Q_OBJECT
public:
    explicit ProcessEngine(int argc, char** argv,QObject *parent = 0);

    void run(bool flag = true);
    void trainTestSplit();

signals:
    void processingDone();

public slots:

protected:
// set of primitives
   QPointer<ProbeRecognizer> probe_rec;
   QStringList app_args;

};

#endif // PROCESSENGINE_H
