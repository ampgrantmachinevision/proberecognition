#include "proberecognizer.h"

ProbeRecognizer::ProbeRecognizer(QObject *parent) : QObject(parent)
{
    //this->lua_interpreter = new QtLuaEngine(); // instantiating the lua interpreter
}

void ProbeRecognizer::loadLuaScriptFiles(QString file_name)
{
    QFile lua_file(file_name);
    if(lua_file.exists()){
        std::cout << "Lua file exists" << std::endl;
    }


    if(lua_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        std::cout << "File Opened" <<std::endl;
    }
    else{
        std::cerr << "Lua file cannot be opened" << std::endl;
    }

    // reading the lua script file
    QTextStream in(&lua_file);

    QString lua_script = in.readAll();

    lua_script_files.push_back(lua_script);

    lua_file.close();

}

void ProbeRecognizer::loadClassLabelFile(QString file)
{
    QFile class_file(file);
    if(class_file.exists()){
        std::cout << "Class file exists" << std::endl;
    }

    if(class_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        std::cout << "File Opened" <<std::endl;
    }
    else{
        std::cerr << "class file cannot be opened" << std::endl;
    }
    QTextStream in(&class_file);
    while(1){
    // reading the lua script file
        QString class_name = in.readLine();
        if(class_name.isEmpty())
            break;
        else
            set_of_classes.push_back(class_name);
    }
    class_file.close();

    // Print out class names
    for(QStringList::iterator it = set_of_classes.begin(); it != set_of_classes.end(); it++){
        std::cout << (*it).toStdString() << std::endl;
    }
}

void ProbeRecognizer::preprocess(cv::Mat &img)
{
    std::vector<cv::Mat> imgSegments = this->imageDivision(img);
    //this->imageResize(img);
    std::vector<cv::Mat> imgSegmentsMasked;
    cv::namedWindow("Masked Regions");
    for(int i=0; i < imgSegments.size(); i++){
        cv::Mat img = imgSegments[i].clone();
        this->imageSegmentation(img);
        imgSegmentsMasked.push_back(img);
    }

}

void ProbeRecognizer::classify(cv::Mat &img)
{
    // CALL THE LUA INTEPRETER TO LOAD THE LUA FILE TO CLASSIFY THE IMAGE.
}

void ProbeRecognizer::createTrainingDataLabels(cv::Mat& img)
{
    // display the image

    // Annotate portions of probe as probe head, probe body, probe stylus and background.

    // save annotated regions as rectangles for image in a single file.
    // format as rect region and label.

    // if annotations are already available for each image, load the annotations.
    // Explore OpenCV annotation tool.

    // divide image into sub-regions, and compare if sub-region has overlap with annotated regions.
    // select label and save sub-image in a specific folder.
}

std::vector<cv::KeyPoint> ProbeRecognizer::featureDetector(cv::Mat &img)
{
    cv::Mat img_gray;
    std::vector<cv::KeyPoint> kps;
    if(img.depth() > 1){ // convert to gray scale
        cv::cvtColor(img,img_gray,cv::COLOR_BGR2GRAY);
    }
    else
        img_gray = img.clone();

   // to be continued.
}

void ProbeRecognizer::imageSegmentation(cv::Mat &img){

// MAYBE USE 'SOL2'
//    // RUN the LUA CODE
//    QString lua_script = lua_script_files[0];

//    std::cout << lua_script.toStdString() << std::endl;

//    // Start the lua interpretor
//    QtLuaEngine* lua_interpreter;

//    if(lua_interpreter.isReady()){
//        std::cout << "The interpretor is ready" << std::endl;
//    }

// RUNNING DEEP MASK CODE IN LUA USING SYSTEM COMMAND
    QProcess th;
    QStringList args;
    QString seg_model_dir = WORKING_DIR;
    seg_model_dir += SEGMENTATION_MODEL_DIR;
    QString img_dir = WORKING_DIR;
    img_dir += DATA_DIR;

    QString inputImage = img_dir + "/sample.jpg";
    QString outputImage = img_dir + "/sample_mask.jpg";
    cv::imwrite(inputImage.toStdString(),img);

    th.setWorkingDirectory(WORKING_DIR); // this will run the lua code in the appropriate directory
    args << "computeProposals.lua" << "-img" << inputImage << "-output" << outputImage << "-np" << QString::number(NUM_PROPOSALS) << seg_model_dir;

    th.start("th",args);
    if(!th.waitForStarted()){
        return;
    }
    else{
        std::cout << "Lua code started" << std::endl;
    }

    if(!th.waitForFinished())
        return;
    else{
        std::cout << "Segmentation code running" << std::endl;
    }
     QByteArray result = th.readAll();
     std::cout << "Lua Output : " << std::endl;
     std::cout << result.toStdString() << std::endl;

     std::cout << "Finished Segmentation" << std::endl;
     img = cv::imread(outputImage.toStdString());
}

std::vector<cv::Mat> ProbeRecognizer::imageDivision(cv::Mat &img)
{
    std::vector<cv::Mat> imgSegments;
    int no_rows = std::floor(img.rows/IMAGENETSIZE);
    int no_cols = std::floor(img.cols/IMAGENETSIZE);

    for(int i = 0 ; i < 2*no_rows-1; i++){
        int y = i*IMAGENETSIZE/2;
        for(int j = 0 ; j < 2*no_cols-1; j++){
            // set the region of interest
            int x = j*IMAGENETSIZE/2;
            cv::Rect roi(x,y,IMAGENETSIZE,IMAGENETSIZE);
            cv::Mat img_local = img(roi);
            cv::Mat img_local_deep_copy = img_local.clone();
            imgSegments.push_back(img_local_deep_copy);
        }
    }

    std::cout << "Number of segments : " << 4*no_rows*no_cols << std::endl;
    return imgSegments;
}

QStringList ProbeRecognizer::getSet_of_classes() const
{
    return set_of_classes;
}
